TRUNCATE TABLE bets RESTART IDENTITY;
insert into bets (date_match, tournament, gamer_1, coeff_1_open, gamer_2, coeff_2_open, coeff_draw_open) values
  ('2016-10-04 10:00:00', 'Dreamhack', 'RoxKis', 1.8,  'NaVi',    1.9, 2.3),
  ('2016-10-04 12:00:00', 'Dreamhack', 'RoxKis', 1.85, 'Cloud9',  1.6, 2.5),
  ('2016-10-04 14:00:00', 'Dreamhack', 'RoxKis', 1.6,  'NaVi',    1.8, 0.0),
  ('2016-10-04 16:00:00', 'Dreamhack', 'NaVi',   2.4,  'Cloud9',  1.7, 2.1),
  ('2016-10-04 18:00:00', 'Dreamhack', 'Cloud9', 1.9,  'NaVi',    2.1, 0.0),
  ('2016-10-05 10:00:00', 'Dreamhack', 'Cloud9', 1.5,  'RoxKis',  1.9, 0.0);