package ru.mskgroup.statandbet.egb.db;

import ru.mskgroup.statandbet.egb.model.Bet;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BetsTestData {
    private static Long count = 1L;
    private static ArrayList<Bet> betList = new ArrayList<>(6);
    private final static String TEAM_ROXKIS = "RoxKis";
    private final static String TEAM_NAVI = "NaVi";
    private final static String TEAM_CLOUD9 = "Cloud9";
    private final static String EVENT_DREAMHACK = "Dreamhack";
    protected final static Bet newBetToSave;

    static {
        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 4, 10, 0))
                .gamer1(TEAM_ROXKIS)
                .coeff1(1.8d)
                .gamer2(TEAM_NAVI)
                .coeff2(1.9d)
                .coeffDraw(2.3d)
                .tournament(EVENT_DREAMHACK)
                .build());

        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 4, 12, 0))
                .gamer1(TEAM_ROXKIS)
                .coeff1(1.85d)
                .gamer2(TEAM_CLOUD9)
                .coeff2(1.6d)
                .coeffDraw(2.5d)
                .tournament(EVENT_DREAMHACK)
                .build());

        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 4, 14, 0))
                .gamer1(TEAM_ROXKIS)
                .coeff1(1.6d)
                .gamer2(TEAM_NAVI)
                .coeff2(1.8d)
                .coeffDraw(0d)
                .tournament(EVENT_DREAMHACK)
                .build());

        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 4, 16, 0))
                .gamer1(TEAM_NAVI)
                .coeff1(2.4d)
                .gamer2(TEAM_CLOUD9)
                .coeff2(1.7d)
                .coeffDraw(2.1d)
                .tournament(EVENT_DREAMHACK)
                .build());

        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 4, 18, 0))
                .gamer1(TEAM_CLOUD9)
                .coeff1(1.9d)
                .gamer2(TEAM_NAVI)
                .coeff2(2.1d)
                .coeffDraw(0d)
                .tournament(EVENT_DREAMHACK)
                .build());

        betList.add(Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 10, 5, 10, 0))
                .gamer1(TEAM_CLOUD9)
                .coeff1(1.5d)
                .gamer2(TEAM_ROXKIS)
                .coeff2(1.9d)
                .coeffDraw(0d)
                .tournament(EVENT_DREAMHACK)
                .build());

        newBetToSave = Bet.builder()
                .id(count++)
                .date(LocalDateTime.of(2016, 11, 26, 22, 0))
                .gamer1(TEAM_ROXKIS)
                .coeff1(1.2d)
                .gamer2(TEAM_NAVI)
                .coeff2(3.4d)
                .coeffDraw(0d)
                .tournament(EVENT_DREAMHACK)
                .build();
    }

    @SuppressWarnings("unchecked")
    protected static List<Bet> getBets() {
        return (List<Bet>)betList.clone();
    }
}
