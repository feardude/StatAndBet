package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mskgroup.statandbet.egb.config.WebSupportConfig;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * User: mkiryanov
 * Time: 31.10.16 22:21
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebSupportConfig.class)
public class CloudFlareHtmlParserTest {

    @Autowired
    private CloudFlareHtmlParser parser;

    @Value("classpath:support/cloudflare/response.html")
    private Resource html;

    @Test
    public void testParseHtml() throws IOException {
        ParseResult result = parser.parse(IOUtils.toString(html.getInputStream(), StandardCharsets.UTF_8));

        assertThat("Invalid challenge parameter value",
                result.getChallenge(), is("9eb39a22b002726a06e3091c3b4e5d4a"));

        assertThat("Invalid pass parameter value",
                result.getPass(), is("1477847450.12-lsP2ND/xdV"));

        assertThat("Invalid action parameter value",
                result.getAction(), is("/cdn-cgi/l/chk_jschl"));

        assertThat("Invalid method parameter value",
                result.getMethod(), is("get"));

        assertThat("Invalid evaluation code", result.getOperation(),
                is("IulbNAd={\"eqpk\":+((+!![]+[])+(!+[]+!![]+!![]+!![]+!![]))};" +
                        "IulbNAd.eqpk*=+((!+[]+!![]+!![]+!![]+[])+(!+[]+!![]+!![]+!![]));" +
                        "IulbNAd.eqpk+=+((!+[]+!![]+[])+(!+[]+!![]));" +
                        "a.value = parseInt(IulbNAd.eqpk, 10) + t.length;"));
    }
}