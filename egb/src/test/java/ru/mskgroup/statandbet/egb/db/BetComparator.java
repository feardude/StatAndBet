package ru.mskgroup.statandbet.egb.db;

import ru.mskgroup.statandbet.egb.model.Bet;
import ru.mskgroup.statandbet.egb.model.BetDto;

import java.util.List;

public class BetComparator {

    protected static boolean equals(Bet bet, BetDto betDto) {

        if (!bet.getId().equals(betDto.getId())) return false;
        if (!bet.getTournament().equals(betDto.getTournament())) return false;
        if (!bet.getGamer1().equals(betDto.getGamer1())) return false;
        return bet.getGamer2().equals(betDto.getGamer2());
    }

    protected static boolean equals(List<Bet> betList, List<BetDto> betDtoList) {
        if (betList.size() != betDtoList.size()) return false;

        boolean isEqual = false;
        for (Bet bet : betList) {
            for (BetDto betDto : betDtoList) {
                isEqual = equals(bet, betDto);
            }
        }
        return isEqual;
    }

}
