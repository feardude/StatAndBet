package ru.mskgroup.statandbet.egb.db;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mskgroup.statandbet.egb.config.PersistenceConfig;
import ru.mskgroup.statandbet.egb.model.Bet;
import ru.mskgroup.statandbet.egb.model.BetDto;

import java.util.List;

import static ru.mskgroup.statandbet.egb.db.BetsTestData.getBets;
import static ru.mskgroup.statandbet.egb.db.BetsTestData.newBetToSave;

@ContextConfiguration(classes = PersistenceConfig.class)
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner.class)
@Sql(scripts = {
        "classpath:db/schema.sql",
        "classpath:db/data.sql"
})
@Ignore
public class JdbcBetDaoTest {

    @Autowired
    private BetDao jdbcBetDao;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();


    @Test
    public void testGetAll() {
        List<BetDto> actual = jdbcBetDao.getAll();
        List<Bet> expected = getBets();

        Assert.assertTrue(BetComparator.equals(expected, actual));
    }

    @Test
    public void testGetById() {
        int id = 4;
        Bet expected = getBets().get(id - 1);
        BetDto actual = jdbcBetDao.getById(id);
        Assert.assertTrue(BetComparator.equals(expected, actual));
    }

    @Test
    public void testSaveNewBet() {
        jdbcBetDao.save(newBetToSave);
        long id = newBetToSave.getId();

        List<BetDto> actualBets = jdbcBetDao.getAll();
        List<Bet> expectedBets = getBets();
        expectedBets.add(newBetToSave);

        Assert.assertEquals(actualBets.size(), expectedBets.size());
        Assert.assertTrue(BetComparator.equals(newBetToSave, jdbcBetDao.getById(id)));
    }

    @Test
    public void testUpdateBet() {
        int id = 4;
        Bet betOld = getBets().get(id-1).clone();

        Bet betUpdated = betOld.clone();
        betUpdated.setCoeff1(betUpdated.getCoeff1() + 1);
        betUpdated.setCoeff2(betUpdated.getCoeff2() + 1);
        betUpdated.setCoeffDraw(betUpdated.getCoeffDraw() + 1);

        Assert.assertTrue(jdbcBetDao.save(betUpdated));
        Assert.assertTrue(BetComparator.equals(betUpdated, jdbcBetDao.getById(id)));
    }

    @Test
    public void testDeleteBet() {
        int id = 4;
        Assert.assertTrue(jdbcBetDao.delete(id));

        List<BetDto> actual = jdbcBetDao.getAll();
        List<Bet> expected = getBets();
        expected.remove(id - 1);

        Assert.assertTrue(BetComparator.equals(expected, actual));

        expectedException.expect(DataAccessException.class);
        jdbcBetDao.getById(id);
    }
}
