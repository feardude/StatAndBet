package ru.mskgroup.statandbet.egb.webservice.support;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import ru.mskgroup.statandbet.egb.config.HttpBaseConfig;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.ResponseDto;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * User: mkiryanov
 * Time: 23.10.16 21:49
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HttpBaseConfig.class)
public class CookieRestTemplateTest {

    @Autowired
    private RestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);

        String betsJson = loadBetsJson();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Set-Cookie", "testCookie");

        mockServer.expect(requestTo("https://egb.com/bets"))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(betsJson, MediaType.APPLICATION_JSON)
                        .headers(headers));
    }

    @Test
    public void shouldCatchCookie() {
        HttpEntity<ResponseDto> response = restTemplate.getForEntity("https://egb.com/bets", ResponseDto.class);

        assertThat("Invalid cookie", response.getHeaders().getFirst("Set-Cookie"), is("testCookie"));
        assertThat("Invalid response model", response.getBody(), notNullValue());
    }

    private String loadBetsJson() throws URISyntaxException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        Path path = Paths.get(classLoader.getResource("rest/egaming/bets.json").toURI());

        return path == null ? null : new String(Files.readAllBytes(path));
    }
}