package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mskgroup.statandbet.egb.config.WebSupportConfig;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebSupportConfig.class)
public class CloudFlareSolverIT {
    @Autowired
    private CloudFlareSolver solver;

    @Value("classpath:support/cloudflare/response.html")
    private Resource html;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldFailIfCallWithEmptyString() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Parameter 'html' must not be null, empty, or blank");

        solver.solve("");
    }

    @Test
    public void shouldSolveTask() throws IOException {
        SolveResult result = solver.solve(IOUtils.toString(html.getInputStream(), StandardCharsets.UTF_8));

        assertThat("Invalid instance", result, notNullValue());
        assertThat("Invalid result answer", result.getAnswer(), is(689));
    }
}
