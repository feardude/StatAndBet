package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * User: mkiryanov
 * Time: 31.10.16 22:37
 */
@Getter
@Setter
@Builder
public class ParseResult {

    private String challenge;
    private String pass;
    private String action;
    private String method;
    private String operation;

}
