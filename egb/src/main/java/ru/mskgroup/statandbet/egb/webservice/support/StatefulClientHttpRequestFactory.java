package ru.mskgroup.statandbet.egb.webservice.support;

import org.apache.http.client.HttpClient;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.net.URI;

/**
 * User: mkiryanov
 * Time: 25.10.16 20:36
 */
public class StatefulClientHttpRequestFactory extends HttpComponentsClientHttpRequestFactory {

    private HttpContext httpContext;

    public StatefulClientHttpRequestFactory(HttpClient httpClient, HttpContext httpContext) {
        super(httpClient);
        this.httpContext = httpContext;
    }

    @Override
    protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
        return httpContext;
    }
}
