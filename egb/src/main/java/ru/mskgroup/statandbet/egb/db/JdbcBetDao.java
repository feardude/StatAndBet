package ru.mskgroup.statandbet.egb.db;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mskgroup.statandbet.egb.model.Bet;
import ru.mskgroup.statandbet.egb.model.BetDto;

import java.sql.Timestamp;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

@Repository
public class JdbcBetDao implements BetDao {

    private static final Logger LOG = getLogger(JdbcBetDao.class);

    private NamedParameterJdbcOperations jdbcTemplate;

    @Autowired
    public JdbcBetDao(NamedParameterJdbcOperations jdbcOperations) {
        this.jdbcTemplate = jdbcOperations;
    }

    @Override
    public boolean save(final Bet bet) {
        LOG.debug("Saving entity {}", bet);
        String sql =
                "INSERT INTO bets (id, date_match, tournament, gamer_1, coeff_1_open, gamer_2, coeff_2_open, coeff_draw_open) " +
                "VALUES (:id, :date_match, :tournament, :gamer_1, :coeff_1, :gamer_2, :coeff_2, :coeff_draw) " +
                "ON CONFLICT (id) DO UPDATE SET " +
                "date_match = :date_match, coeff_1_close = :coeff_1, coeff_2_close = :coeff_2, coeff_draw_close = :coeff_draw " +
                "WHERE bets.id = :id";
        return jdbcTemplate.update(sql, createParameterSource(bet)) == 1;
    }

    @Override
    public BetDto getById(final long id) {
        String sql = "SELECT * FROM bets WHERE id = :id";
        final SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        return jdbcTemplate.queryForObject(sql, parameterSource, createRowMapper());
    }

    @Override
    public List<BetDto> getAll() {
        String sql = "SELECT * FROM bets";
        return jdbcTemplate.query(sql, createRowMapper());
    }

    @Override
    public boolean delete(final long id) {
        String sql = "DELETE FROM bets WHERE id = :id";
        final SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        return jdbcTemplate.update(sql, parameterSource) == 1;
    }

    private static MapSqlParameterSource createParameterSource(Bet bet) {
        return new MapSqlParameterSource()
                .addValue("id", bet.getId())
                .addValue("date_match", Timestamp.valueOf(bet.getDate()))
                .addValue("tournament", bet.getTournament())
                .addValue("gamer_1", bet.getGamer1())
                .addValue("gamer_2", bet.getGamer2())
                .addValue("coeff_1", bet.getCoeff1())
                .addValue("coeff_2", bet.getCoeff2())
                .addValue("coeff_draw", bet.getCoeffDraw());
    }

    private static RowMapper<BetDto> createRowMapper() {
        return (rs, rowNum) -> {
            BetDto bet = new BetDto();
            bet.setId(rs.getLong("id"));
            bet.setDateMatch(rs.getTimestamp("date_match").toLocalDateTime());
            bet.setTournament(rs.getString("tournament"));
            bet.setGamer1(rs.getString("gamer_1"));
            bet.setGamer2(rs.getString("gamer_2"));
            bet.setDateCoeffOpen(rs.getTimestamp("date_coeff_open").toLocalDateTime());
            bet.setCoeff1Open(rs.getDouble("coeff_1_open"));
            bet.setCoeff2Open(rs.getDouble("coeff_2_open"));
            bet.setCoeffDrawOpen(rs.getDouble("coeff_draw_open"));
            bet.setCoeff1Close(rs.getDouble("coeff_1_close"));
            bet.setCoeff2Close(rs.getDouble("coeff_2_close"));
            bet.setCoeffDrawClose(rs.getDouble("coeff_draw_close"));
            return bet;
        };
    }
}