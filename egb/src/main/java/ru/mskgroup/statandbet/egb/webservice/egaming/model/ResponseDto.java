package ru.mskgroup.statandbet.egb.webservice.egaming.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * User: mkiryanov
 * Time: 22.11.16 21:58
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class ResponseDto {

    private List<JsonBet> bets;

}
