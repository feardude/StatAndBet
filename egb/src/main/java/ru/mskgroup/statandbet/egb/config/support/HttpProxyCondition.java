package ru.mskgroup.statandbet.egb.config.support;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * @author maksim-kiryanov
 */
public class HttpProxyCondition implements Condition {
    @Override
    public boolean matches(ConditionContext conditionContext,
                           AnnotatedTypeMetadata annotatedTypeMetadata) {
        ProxyMode mode = conditionContext.getEnvironment()
                .getProperty(ProxyProperties.PROXY_MODE_NAME, ProxyMode.class);
        return mode == ProxyMode.ON;
    }
}
