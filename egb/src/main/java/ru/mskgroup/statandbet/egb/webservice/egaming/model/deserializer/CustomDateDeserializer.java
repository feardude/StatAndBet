package ru.mskgroup.statandbet.egb.webservice.egaming.model.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * User: Maxim Kiryanov
 * Time: 28.11.2016 23:31
 */
public class CustomDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        long timeInMillis = 0;

        JsonToken token = jsonParser.getCurrentToken();
        if (JsonToken.VALUE_NUMBER_INT.equals(token)) {
            long timeInSeconds = Long.valueOf(jsonParser.getText().trim());
            timeInMillis = TimeUnit.MILLISECONDS.convert(timeInSeconds, TimeUnit.SECONDS);
        }

        return new Date(timeInMillis);
    }
}
