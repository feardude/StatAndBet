package ru.mskgroup.statandbet.egb.config.support;

import lombok.Getter;

/**
 * @author maksim-kiryanov
 */
@Getter
public class ProxyProperties {
    public static final String PROXY_MODE_NAME = "PROXY_MODE";
    private ProxyMode mode;
    private String host;
    private Integer port;
    private String login;
    private String password;

    private ProxyProperties(ProxyMode mode) {
        this.mode = mode;
    }

    private ProxyProperties(ProxyMode mode, String host, Integer port, String login, String password) {
        this(mode);
        this.host = host;
        this.port = port;
        this.login = login;
        this.password = password;
    }

    public static ProxyProperties newProxyProperties(String host, Integer port, String login, String password) {
        return new ProxyProperties(ProxyMode.ON, host, port, login, password);
    }

    public static ProxyProperties newOffProxyProperties() {
        return new ProxyProperties(ProxyMode.OFF);
    }
}
