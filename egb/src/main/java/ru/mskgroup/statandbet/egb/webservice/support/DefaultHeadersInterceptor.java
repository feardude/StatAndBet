package ru.mskgroup.statandbet.egb.webservice.support;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.util.Map;

/**
 * User: mkiryanov
 * Time: 22.11.16 22:49
 */
@AllArgsConstructor
public class DefaultHeadersInterceptor implements ClientHttpRequestInterceptor {

    private final Map<String, String> headers;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        for (String header : headers.keySet()) {
            request.getHeaders().add(header, headers.get(header));
        }

        return execution.execute(request, body);
    }
}
