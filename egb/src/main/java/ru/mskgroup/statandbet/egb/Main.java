package ru.mskgroup.statandbet.egb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.mskgroup.statandbet.egb.db.JdbcBetDao;
import ru.mskgroup.statandbet.egb.util.BetAdapter;
import ru.mskgroup.statandbet.egb.webservice.egaming.client.WebClient;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.JsonBet;

import java.util.List;

@SpringBootApplication
@Slf4j
public class Main {

    @Autowired
    private JdbcBetDao betRepository;

    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }

    @Bean
    public CommandLineRunner run(WebClient egbWebClient) throws Exception {
        return args -> {
            log.debug("Try get jsonBets");
            List<JsonBet> jsonBets = egbWebClient.getBets();
            log.debug("Bets: {}", jsonBets);

            log.info("Persisting {} bets", jsonBets.size());
            for (JsonBet jsonBet : jsonBets) {
                betRepository.save(BetAdapter.jsonToBet(jsonBet));
            }

        };
    }
}