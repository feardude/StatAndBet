package ru.mskgroup.statandbet.egb.db;

import ru.mskgroup.statandbet.egb.model.BetDto;
import ru.mskgroup.statandbet.egb.model.Bet;

import java.util.List;

public interface BetDao {

    boolean save(Bet bet);

    BetDto getById(long id);

    List<BetDto> getAll();

    boolean delete(long id);

}
