package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class HackCloudFlareScrapeShield {

    private static final String DOMAIN = "egb.com";

    @Autowired
    private CloudFlareSolver cloudFlareSolver;

    public boolean hack(RestTemplate restTemplate, String errorResponseBody) {
        log.debug("First response: {}", errorResponseBody);
        log.info("Trying to hack CloudFlare antibot shield");
        try {
            log.info("Waiting for 5 seconds");
            TimeUnit.MILLISECONDS.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SolveResult solveResult = cloudFlareSolver.solve(errorResponseBody);
        String answer = String.valueOf(solveResult.getAnswer());

        final List<NameValuePair> params = new ArrayList<>(3);
        params.add(new BasicNameValuePair("jschl_vc", solveResult.getChallenge()));
        params.add(new BasicNameValuePair("pass", solveResult.getPass()));
        params.add(new BasicNameValuePair("jschl_answer", answer));
        log.debug("Parameters: {}", Arrays.toString(params.toArray()));

        Map<String, String> headers = new HashMap<>(1);
        headers.put("Referer", "https://" + DOMAIN + "/");
        String url = "https://" + DOMAIN + "/cdn-cgi/l/chk_jschl?" + URLEncodedUtils.format(params, StandardCharsets.UTF_8);
        log.debug("Hack URL: {}", url);

        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class, headers);
            log.debug("Response on hack: {}", response.toString());
            log.info("CloudFlare antibot shield was successfully hacked");
            return response.getStatusCode().is2xxSuccessful();

        } catch (HttpServerErrorException e) {
            log.debug("Error response: {}", e.getResponseBodyAsString());
            log.info("Error occured on hacking, got response status {}", e.getRawStatusCode());
            return false;
        }
    }

}
