package ru.mskgroup.statandbet.egb.webservice.egaming.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.mskgroup.statandbet.egb.config.qualifier.CookieRest;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.JsonBet;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.ResponseDto;
import ru.mskgroup.statandbet.egb.webservice.support.DefaultHeadersInterceptor;
import ru.mskgroup.statandbet.egb.webservice.support.LoggedClientHttpRequestInterceptor;
import ru.mskgroup.statandbet.egb.webservice.support.cloudflare.HackCloudFlareScrapeShield;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * User: mkiryanov
 * Time: 17.10.16 20:53
 */
@Component
@Slf4j
public class WebClient {

    private static final String PRIMARY_SCHEME = "https";
    private static final String HOST = "egb.com";
    private static final String BETS_PATH = "/bets";

    private RestTemplate restTemplate;

    @Autowired
    private HackCloudFlareScrapeShield hackCloudFlareScrapeShield;

    @Autowired
    public WebClient(@CookieRest RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    public void init() {
        initRestTemplate();
    }

    private void initRestTemplate() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(getMapper());
        restTemplate.getMessageConverters().add(0, converter);

        restTemplate.setInterceptors(createInterceptors());
    }

    private List<ClientHttpRequestInterceptor> createInterceptors() {
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

        interceptors.add(new DefaultHeadersInterceptor(createDefaultHeaderMap()));
        interceptors.add(new LoggedClientHttpRequestInterceptor());

        return interceptors;
    }

    private Map<String, String> createDefaultHeaderMap() {
        Map<String, String> result = new HashMap<>();
        result.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        result.put("Proxy-Connection", "keep-alive");
        return result;
    }

    private ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper;
    }

    public List<JsonBet> getBets() throws HttpServerErrorException {
        ResponseDto response;
        try {
            URI getBetsURI = createGetBetsUri();
            log.info("Getting bets from {}", getBetsURI.toString());
            response = restTemplate.getForObject(getBetsURI, ResponseDto.class);

        } catch (HttpServerErrorException e) {
            log.info("CloudFlare antibot shield was encountered");
            if (!hackCloudFlareScrapeShield.hack(restTemplate, e.getResponseBodyAsString())) {
                log.error("Can't hack CloudFlare antibot shield");
                log.debug(e.getResponseBodyAsString());
                throw e;
            }
            response = restTemplate.getForObject(createGetBetsUri(), ResponseDto.class);
            log.info("Bets were fetched successfully");
        }

        return response.getBets().stream()
                .filter(b -> b.getGame().equals("Counter-Strike"))
                .collect(Collectors.toList());
    }

    private URI createGetBetsUri() {
        return UriComponentsBuilder.newInstance()
                .scheme(PRIMARY_SCHEME).host(HOST).path(BETS_PATH)
                .build().toUri();
    }
}
