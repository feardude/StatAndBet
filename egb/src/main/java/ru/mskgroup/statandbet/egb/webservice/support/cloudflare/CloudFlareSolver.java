package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

@Component
public class CloudFlareSolver {
    private CloudFlareHtmlParser parser;

    @Autowired
    public CloudFlareSolver(CloudFlareHtmlParser parser) {
        this.parser = parser;
    }

    public SolveResult solve(String html) {
        ParseResult parseResult = parser.parse(html);
        Integer answer = calculate(parseResult.getOperation());
        return SolveResult.builder()
                .answer(answer)
                .challenge(parseResult.getChallenge())
                .pass(parseResult.getPass())
                .build();
    }

    private Integer calculate(String input) {
        ScriptEngine engine = new NashornScriptEngineFactory().getScriptEngine();

        try {
            String code = preProcessCode(input);
            return ((Double) engine.eval(code)).intValue();
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
    }

    private String preProcessCode(String input) {
        String code = input.replaceFirst("a.value =", "");
        return code.replaceFirst("t\\.length", "7");
    }
}
