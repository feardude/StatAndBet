package ru.mskgroup.statandbet.egb.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class BetDto extends Bet {
    private LocalDateTime dateMatch;
    private LocalDateTime dateCoeffOpen;
    private Double coeff1Open;
    private Double coeff2Open;
    private Double coeff1Close;
    private Double coeff2Close;
    private Double coeffDrawOpen;
    private Double coeffDrawClose;


    public BetDto() {
        this(LocalDateTime.now(), "default", 0.0d, "default", 0.0d, 0.0d, "default");
    }

    private BetDto(LocalDateTime dateMatch, String gamer1, double coeff1Open,
               String gamer2, double coeff2Open, double coeffDrawOpen, String tournament) {
        this.dateMatch = dateMatch;
        this.dateCoeffOpen = LocalDateTime.now();
        this.gamer1 = gamer1;
        this.gamer2 = gamer2;
        this.tournament = tournament;
        this.coeff1Open = coeff1Open;
        this.coeff2Open = coeff2Open;
        this.coeffDrawOpen = coeffDrawOpen;
        this.coeff1Close = 0d;
        this.coeff2Close = 0d;
        this.coeffDrawClose = 0d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BetDto bet = (BetDto) o;

        if (!dateMatch.equals(bet.dateMatch)) return false;
        if (!tournament.equals(bet.tournament)) return false;
        if (!gamer1.equals(bet.gamer1)) return false;
        if (!gamer2.equals(bet.gamer2)) return false;
        return id.equals(bet.id);
    }

    @Override
    public int hashCode() {
        int result = dateMatch.hashCode();
        result = 31 * result + tournament.hashCode();
        result = 31 * result + gamer1.hashCode();
        result = 31 * result + gamer2.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }
}
