package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class CloudFlareHtmlParser {
    private static final Pattern CHALLENGE_PATTERN = Pattern.compile("name=\"jschl_vc\" value=\"(.+)\"");
    private static final Pattern PASS_PATTERN = Pattern.compile("name=\"pass\" value=\"(.+)\"");
    private static final Pattern FORM_PATTERN = Pattern.compile("form.*action=\"(.+)\".*method=\"(.+)\"");
    private static final Pattern OPERATION_PATTERN = Pattern.compile("(var s,t,o,p,b,r,e,a,k,i,n,g,f, (.+;))|(\\s+;(.+;)) '");

    protected ParseResult parse(String html) {
        Assert.hasText(html, "Parameter 'html' must not be null, empty, or blank");
        log.debug(html);

        Matcher challengeSearch = CHALLENGE_PATTERN.matcher(html);
        Matcher passSearch = PASS_PATTERN.matcher(html);
        Matcher formSearch = FORM_PATTERN.matcher(html);
        Matcher operationSearch = OPERATION_PATTERN.matcher(html);

        if (!challengeSearch.find() || !passSearch.find() || !formSearch.find()) {
            throw new RuntimeException("[Parse failed] Can not find obligatory parameters");
        }

        String challenge = challengeSearch.group(1);
        String pass = passSearch.group(1);
        String action = formSearch.group(1);
        String method = formSearch.group(2);

        String operation = null;
        if (operationSearch.find()) {
            operation = operationSearch.group(2);
        }
        if (operationSearch.find()) {
            operation += operationSearch.group(4);
        }

        return ParseResult.builder()
                .challenge(challenge)
                .pass(pass)
                .action(action)
                .method(method)
                .operation(operation)
                .build();
    }
}
