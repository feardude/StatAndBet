package ru.mskgroup.statandbet.egb.config;

import org.apache.http.client.AuthCache;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import ru.mskgroup.statandbet.egb.config.qualifier.CookieRest;
import ru.mskgroup.statandbet.egb.webservice.support.StatefulClientHttpRequestFactory;

/**
 * User: mkiryanov
 * Time: 23.10.16 22:53
 */
@Configuration
public class HttpBaseConfig {
    private static final int DEFAULT_TIMEOUT_MILLISECONDS = (2 * 1000);

    @Bean
    public CookieStore cookieStore() {
        return new BasicCookieStore();
    }

    @Bean
    public HttpClientContext httpContext(CookieStore cookieStore) {
        return newHttpClientContext(cookieStore);
    }

    static HttpClientContext newHttpClientContext(CookieStore cookieStore) {
        AuthCache authCache = new BasicAuthCache();

        HttpClientContext httpContext = HttpClientContext.create();
        httpContext.setCookieStore(cookieStore);
        httpContext.setAuthCache(authCache);
        return httpContext;
    }

    @Bean
    public HttpClient httpClient(CookieStore cookieStore) {
        return newHttpClientBuilder(cookieStore).build();
    }

    static HttpClientBuilder newHttpClientBuilder(CookieStore cookieStore) {
        return HttpClients.custom()
                .setDefaultCookieStore(cookieStore);
    }

    @Bean
    public ClientHttpRequestFactory cookieHttpRequestFactory(HttpClient httpClient, HttpContext httpContext) {
        StatefulClientHttpRequestFactory factory =
                new StatefulClientHttpRequestFactory(httpClient, httpContext);
        factory.setConnectTimeout(DEFAULT_TIMEOUT_MILLISECONDS);
        factory.setReadTimeout(DEFAULT_TIMEOUT_MILLISECONDS);
        return factory;
    }

    @Bean
    @CookieRest
    public RestTemplate cookieRestTemplate(ClientHttpRequestFactory httpRequestFactory) {
        return new RestTemplate(httpRequestFactory);
    }
}
