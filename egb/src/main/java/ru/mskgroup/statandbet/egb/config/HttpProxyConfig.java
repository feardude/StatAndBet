package ru.mskgroup.statandbet.egb.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import ru.mskgroup.statandbet.egb.config.support.HttpProxyCondition;
import ru.mskgroup.statandbet.egb.config.support.ProxyMode;
import ru.mskgroup.statandbet.egb.config.support.ProxyProperties;

/**
 * @author maksim-kiryanov
 */
@Configuration
@Conditional(HttpProxyCondition.class)
public class HttpProxyConfig {
    @Bean
    public ProxyProperties proxyProps(Environment env) {
        ProxyMode mode = env.getProperty(ProxyProperties.PROXY_MODE_NAME, ProxyMode.class);
        if (mode == ProxyMode.ON) {
            return ProxyProperties.newProxyProperties(
                    env.getRequiredProperty("PROXY_HOST"),
                    env.getRequiredProperty("PROXY_PORT", Integer.class),
                    env.getRequiredProperty("PROXY_LOGIN"),
                    env.getRequiredProperty("PROXY_PASSWORD"));
        }

        return ProxyProperties.newOffProxyProperties();
    }

    @Bean
    public Credentials credentials(ProxyProperties proxyProps) {
        return new UsernamePasswordCredentials(proxyProps.getLogin(), proxyProps.getPassword());
    }

    @Bean
    public CredentialsProvider credentialsProvider(ProxyProperties proxyProps,
                                                   Credentials credentials) {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        AuthScope authscope = new AuthScope(proxyProps.getHost(), proxyProps.getPort());
        credentialsProvider.setCredentials(authscope, credentials);
        return credentialsProvider;
    }


    @Bean
    @Primary
    public HttpContext httpProxyContext(CredentialsProvider credentialsProvider, CookieStore cookieStore) {
        HttpClientContext httpContext = HttpBaseConfig.newHttpClientContext(cookieStore);
        httpContext.setCredentialsProvider(credentialsProvider);
        return httpContext;
    }

    @Bean
    @Primary
    public HttpClient httpProxyClient(ProxyProperties proxyProps,
                                      CredentialsProvider credentialsProvider, CookieStore cookieStore) {
        HttpHost proxy = new HttpHost(proxyProps.getHost(), proxyProps.getPort());
        DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);

        return HttpBaseConfig.newHttpClientBuilder(cookieStore)
                .setDefaultCredentialsProvider(credentialsProvider)
                .setRoutePlanner(routePlanner)
                .build();
    }
}
