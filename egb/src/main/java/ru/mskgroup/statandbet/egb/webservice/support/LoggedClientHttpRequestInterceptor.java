package ru.mskgroup.statandbet.egb.webservice.support;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * User: mkiryanov
 * Time: 22.11.16 22:17
 */
@Slf4j
public class LoggedClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {
        traceRequest(request);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    private void traceRequest(HttpRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("Send {} request to {} with headers\n", request.getMethod(), request.getURI());
            traceHeaders(request.getHeaders());
        }
    }

    private void traceResponse(ClientHttpResponse response) {
        try {
            if (log.isDebugEnabled()) {
                log.debug("Retrieve response {} {} with headers\n", response.getRawStatusCode(), response.getStatusText());
                traceHeaders(response.getHeaders());
            }
        } catch (IOException e) {
            log.error("Exception while access to response", e);
        }
    }

    private void traceHeaders(HttpHeaders headers) {
        for (String header : headers.keySet()) {
            for (String value : headers.get(header)) {
                log.debug("{}: {}", header, value);
            }
        }
    }
}
