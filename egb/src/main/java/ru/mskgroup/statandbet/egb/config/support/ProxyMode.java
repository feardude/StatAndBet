package ru.mskgroup.statandbet.egb.config.support;

/**
 * @author maksim-kiryanov
 */
public enum ProxyMode {
    ON, OFF
}
