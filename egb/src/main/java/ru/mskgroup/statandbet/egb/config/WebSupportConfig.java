package ru.mskgroup.statandbet.egb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.mskgroup.statandbet.egb.webservice.support.cloudflare.CloudFlareHtmlParser;
import ru.mskgroup.statandbet.egb.webservice.support.cloudflare.CloudFlareSolver;

//TODO mkiryanov: удалить конфиг, переделать тесты
@Configuration
public class WebSupportConfig {
    @Bean
    public CloudFlareHtmlParser cloudFlareHtmlParser() {
        return new CloudFlareHtmlParser();
    }

    @Bean
    public CloudFlareSolver cloudFlareSolver(CloudFlareHtmlParser parser) {
        return new CloudFlareSolver(parser);
    }
}
