package ru.mskgroup.statandbet.egb.webservice.egaming.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.deserializer.CustomDateDeserializer;

import java.util.Date;

/**
 * User: mkiryanov
 * Time: 17.10.16 20:35
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class JsonBet {

    private Long id;
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date date;
    private String game;
    @JsonProperty("tourn")
    private String tournament;
    @JsonProperty("coef_1")
    private String coefficient1;
    @JsonProperty("gamer_1")
    private Gamer gamer1;
    @JsonProperty("coef_2")
    private String coefficient2;
    @JsonProperty("gamer_2")
    private Gamer gamer2;

}
