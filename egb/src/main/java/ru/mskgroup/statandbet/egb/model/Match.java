package ru.mskgroup.statandbet.egb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Match {

    private Long id;
    private String event;
    private String team1;
    private String team2;
    private String date;
    private String url;

}
