package ru.mskgroup.statandbet.egb.util;

import lombok.NoArgsConstructor;
import ru.mskgroup.statandbet.egb.model.Bet;
import ru.mskgroup.statandbet.egb.webservice.egaming.model.JsonBet;

import java.time.ZoneOffset;

import static java.lang.Double.parseDouble;
import static java.time.LocalDateTime.ofInstant;

@NoArgsConstructor
public class BetAdapter {

    public static Bet jsonToBet(final JsonBet jsonBet) {
        return Bet.builder()
                .coeff1(parseDouble(jsonBet.getCoefficient1()))
                .coeff2(parseDouble(jsonBet.getCoefficient2()))
                .coeffDraw(0d)
                .gamer1(jsonBet.getGamer1().getName())
                .gamer2(jsonBet.getGamer2().getName())
                .date(ofInstant(jsonBet.getDate().toInstant(), ZoneOffset.UTC))
                .tournament(jsonBet.getTournament())
                .id(jsonBet.getId())
                .build();
    }
}
