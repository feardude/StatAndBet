package ru.mskgroup.statandbet.egb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class Bet implements Cloneable {

    protected Long id;
    private LocalDateTime date;
    protected String tournament;
    protected String gamer1;
    protected String gamer2;
    private Double coeff1;
    private Double coeff2;
    private Double coeffDraw;


    public Bet() {
        this(0L, LocalDateTime.now(), "default", "default", "default", 0.0d, 0.0d,0.0d);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bet bet = (Bet) o;

        if (!date.equals(bet.date)) return false;
        if (!tournament.equals(bet.tournament)) return false;
        if (!gamer1.equals(bet.gamer1)) return false;
        if (!gamer2.equals(bet.gamer2)) return false;
        return id.equals(bet.id);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + tournament.hashCode();
        result = 31 * result + gamer1.hashCode();
        result = 31 * result + gamer2.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    public Bet clone() {
        try {
            return (Bet) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
