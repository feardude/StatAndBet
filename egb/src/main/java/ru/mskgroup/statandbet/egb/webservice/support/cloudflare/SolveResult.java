package ru.mskgroup.statandbet.egb.webservice.support.cloudflare;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * User: mkiryanov
 * Time: 01.11.16 0:31
 */
@Getter
@Setter
@Builder
public class SolveResult {

    private String challenge;
    private String pass;
    private Integer answer;

}
