package ru.mskgroup.statandbet.egb.webservice.egaming.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * User: mkiryanov
 * Time: 17.10.16 20:42
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class Gamer {

    @JsonProperty("nick")
    private String name;
    private Long win;
    private Long points;

}
