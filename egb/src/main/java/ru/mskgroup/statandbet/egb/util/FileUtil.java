package ru.mskgroup.statandbet.egb.util;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Slf4j
public class FileUtil {

    public static void listToCSV(String path, List<?> list) {

        if (list == null || list.size() == 0 || path == null || path.isEmpty()) {
            log.warn("Empty input parameters. Cannot write info to .csv file");
            return;
        }

        File file = new File(path);
        try {
            if (file.createNewFile()) {
                log.info("File created: {}", path);
            } else {
                log.info("File already exists: {}", path);
            }
        } catch (IOException e) {
            log.error("Error while creating file {}: {}", path, e.toString());
        }

        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
//            writer.write(list.get(0).getCSVHeader());
            for (Object t : list) {
                writer.newLine();
//                writer.write(t.toCSV());
            }

        } catch (IOException e) {
            log.error("Caught IOException: {}", e.toString());
        }
    }

}
