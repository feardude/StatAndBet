package ru.mskgroup.statandbet.egb.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class PersistenceConfig {
    @Bean(name = "dataSource")
    public BasicDataSource inMemoryDataSource() {
        return configureDataSource(
                "jdbc:postgresql://localhost:5432/statandbet",
                "postgres",
                "admin",
                "");
    }

    private BasicDataSource configureDataSource(String url, String username,
                                                String password, String connectionProperties) {
        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setInitialSize(20);
        ds.setMaxActive(10);
        ds.setConnectionProperties(connectionProperties);

        return ds;
    }

    @Bean
    public NamedParameterJdbcOperations namedJdbcTemplate(@Qualifier("dataSource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
