insert into bets (date_match, tournament, gamer_1, coeff_1_open, gamer_2, coeff_2_open, coeff_draw_open) values
  ('2016-10-04 10:00:00', 'Asus cup', 'team1', 1.675, 'team2', 3.417, 2.062),
  ('2016-10-04 12:00:00', 'Asus cup', 'team3', 2.884, 'team4', 1.896, 0.000),
  ('2016-10-04 14:00:00', 'Asus cup', 'team5', 3.310, 'team6', 1.972, 2.123),
  ('2016-10-04 16:00:00', 'Asus cup', 'team7', 2.745, 'team8', 1.651, 2.348),
  ('2016-10-04 18:00:00', 'MSI',      'team1', 3.289, 'team4', 1.893, 2.607),
  ('2016-10-05 10:00:00', 'MSI',      'team3', 1.378, 'team2', 2.413, 0.000);