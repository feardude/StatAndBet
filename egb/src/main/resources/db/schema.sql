DROP TABLE IF EXISTS bets;

CREATE TABLE bets (
  id SERIAL PRIMARY KEY,
  date_match TIMESTAMP NOT NULL,
  tournament TEXT NOT NULL,
  gamer_1 TEXT NOT NULL,
  gamer_2 TEXT NOT NULL,
  date_coeff_open TIMESTAMP DEFAULT CURRENT_TIMESTAMP(0),
  coeff_1_open DECIMAL NOT NULL,
  coeff_2_open DECIMAL NOT NULL,
  coeff_draw_open DECIMAL DEFAULT 0,
  coeff_1_close DECIMAL DEFAULT 0,
  coeff_2_close DECIMAL DEFAULT 0,
  coeff_draw_close DECIMAL DEFAULT 0
);