package ru.mskgroup.commons;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static java.lang.Long.parseLong;
import static java.time.Instant.ofEpochMilli;

public class DateUtils {

    public static LocalDateTime unixToLocalDateTime(String unixTimestamp) {
        return ofEpochMilli(parseLong(unixTimestamp))
                .atZone(ZoneOffset.UTC)
                .toLocalDateTime();
    }

    public static LocalDate formatSnookerBirthDate(String input) {
        String[] date = input.split(" ");           // December, 5th, 1975
        return LocalDate.parse(String.format("%s-%s-%s",
                date[2],
                formatMonth(date[0].substring(0, 3)),
                formatDay(date[1].split("[^0-9]")[0])
        ));
    }

    private static String formatMonth(String input) {
        switch (input.toLowerCase()) {
            case ("jan"): return "01";
            case ("feb"): return "02";
            case ("mar"): return "03";
            case ("apr"): return "04";
            case ("may"): return "05";
            case ("jun"): return "06";
            case ("jul"): return "07";
            case ("aug"): return "08";
            case ("sep"): return "09";
            case ("oct"): return "10";
            case ("nov"): return "11";
            case ("dec"): return "12";
            default: return "01";
        }
    }

    private static String formatDay(String input) {
        return input.length() == 1 ? "0" + input : input;
    }

}
