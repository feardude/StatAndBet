package ru.mskgroup.commons;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class JsoupUtil {
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) " +
            "Chrome/23.0.1271.95 Safari/537.11";
    private static final int MAX_ATTEMPTS = 5;

    /**
     * Fetches Html Document for the input URL.
     * The method tries to connect 5 times, if the URL is not available to be connected, return null.
     *
     * @param url
     * @return Fetched document in case of success connection. null if couldn't connect
     */
    public static Document getHtmlFromURL(String url) {
        int currentAttempt = 1;

        while (currentAttempt <= MAX_ATTEMPTS) {
            try {
                return Jsoup.connect(url)
                        .timeout(15000)
                        .userAgent(USER_AGENT)
                        .get();
            } catch (IOException e) {
                currentAttempt++;
                log.error("Error connecting to URL {} Trying {} time.", url, currentAttempt, e);
            }
        }

        return null;
    }
}
