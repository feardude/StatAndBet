package ru.mskgroup.statandbet.hltv.loader;

import org.junit.Test;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchPersistence;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.service.HltvService;

import java.util.List;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class MatchPersistenceTest {
    private HltvService hltvService = mock(HltvService.class);
    private MatchPersistence matchPersistence = new MatchPersistence(hltvService);

    @Test
    public void noDuplicatesSave() {
        when(hltvService.countDuplicateMatchIds(singletonList("-10_0")))
                .thenReturn(0);

        final List<Match> matches = singletonList(Match.builder().hltvMatchId(-10).ordinal(0).build());
        matchPersistence.save(matches);

        verify(hltvService).countDuplicateMatchIds(singletonList("-10_0"));
        verify(hltvService).saveMatches(matches);
        verifyNoMoreInteractions(hltvService);
    }

    @Test
    public void duplicatesNoSave() {
        when(hltvService.countDuplicateMatchIds(singletonList("1_0")))
                .thenReturn(1);
        when(hltvService.getMatchUniqueIds())
                .thenReturn(singletonList("1_0"));

        final List<Match> matches = singletonList(Match.builder().hltvMatchId(1).ordinal(0).build());
        matchPersistence.save(matches);

        verify(hltvService).countDuplicateMatchIds(singletonList("1_0"));
        verify(hltvService).getMatchUniqueIds();
        verifyNoMoreInteractions(hltvService);
    }

    @Test
    public void duplicatesSave() {
        when(hltvService.countDuplicateMatchIds(asList("1_0", "2_0")))
                .thenReturn(1);
        when(hltvService.getMatchUniqueIds())
                .thenReturn(singletonList("1_0"));

        final List<Match> matches = readJsonList("hltv/loader/persistence/save_duplicates/matches.json", Match.class);
        matchPersistence.save(matches);

        final List<Match> expected = readJsonList("hltv/loader/persistence/save_duplicates/expected.json", Match.class);
        verify(hltvService).countDuplicateMatchIds(asList("1_0", "2_0"));
        verify(hltvService).getMatchUniqueIds();
        verify(hltvService).saveMatches(expected);
        verifyNoMoreInteractions(hltvService);
    }

    @Test
    public void saveHandleDateTimeDuplicates() {
        final List<Match> matches = readJsonList("hltv/loader/persistence/save_upd_datetime/matches.json", Match.class);

        matchPersistence.save(matches);

        final List<Match> expected = readJsonList("hltv/loader/persistence/save_upd_datetime/expected.json", Match.class);
        verify(hltvService).countDuplicateMatchIds(asList("1_0", "2_0"));
        verify(hltvService).saveMatches(expected);
        verifyNoMoreInteractions(hltvService);
    }

    @Test
    public void saveSameHltvMatchId() {
        final List<Match> matches = readJsonList("hltv/loader/persistence/save_same_hltv_match_id/matches.json", Match.class);

        matchPersistence.save(matches);

        verify(hltvService).countDuplicateMatchIds(asList("1_0", "1_1"));
        verify(hltvService).saveMatches(matches);
        verifyNoMoreInteractions(hltvService);
    }
}
