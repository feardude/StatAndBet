package ru.mskgroup.statandbet.hltv.service.dao;

import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.mskgroup.statandbet.hltv.service.TestUtils;

import java.util.List;

import static java.lang.String.format;
import static org.junit.Assert.assertTrue;
import static ru.mskgroup.statandbet.hltv.service.TestUtils.checkTestResourceFileExists;

@ContextConfiguration(classes = HltvDaoTestConfig.class)
@Slf4j
@Transactional
@Rollback
public class AbstractDaoIT {
    @BeforeClass
    public static void checkTestDbCredentials() {
        final String filePath = "datasource.properties";
        checkTestResourceFileExists(filePath);

        final String expected = "hltv.jdbc.username=hltv_test";
        final List<String> dbProps = TestUtils.readLines(filePath);
        final String message = format("Test resource file '%s' must contain '%s'", filePath, expected);
        assertTrue(message, dbProps.contains(expected));
    }
}
