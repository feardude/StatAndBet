package ru.mskgroup.statandbet.hltv.loader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.mskgroup.commons.JsoupUtil;
import ru.mskgroup.statandbet.hltv.loader.parsing.ResultsParser;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;

import java.util.List;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static com.feardude.commons.ResourceUtils.readResourceAsString;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JsoupUtil.class)
@PowerMockIgnore("javax.management.*")
public class ResultsParserTest {
    private static final String RESULTS_URL = "https://www.hltv.org/results?startDate=2012-01-01&endDate=2012-12-31";
    private static final String RESULTS_URL_PATTERN = "/results?startDate=%d-01-01&endDate=%d-12-31";

    private final ResultsParser parser = new ResultsParser(RESULTS_URL_PATTERN, singletonList(2012));

    @Before
    public void initStaticMock() {
        PowerMockito.mockStatic(JsoupUtil.class);
    }

    @After
    public void verifyMock() {
        PowerMockito.verifyStatic(JsoupUtil.class);
        JsoupUtil.getHtmlFromURL(RESULTS_URL);
    }

    @Test
    public void getHtmlNull() {
        PowerMockito.when(JsoupUtil.getHtmlFromURL(RESULTS_URL))
                .thenReturn(null);

        final List<MatchUrl> expected = emptyList();
        final List<MatchUrl> actual = parser.parse();

        assertEquals(expected, actual);
    }

    @Test
    public void loadOnePage() {
        final String html = readResourceAsString("hltv/loader/results_parser/load_one_page/document.html");
        final Document document = Jsoup.parse(html);

        PowerMockito.when(JsoupUtil.getHtmlFromURL(RESULTS_URL))
                .thenReturn(document);

        final List<MatchUrl> expected = readJsonList("hltv/loader/results_parser/load_one_page/expected.json", MatchUrl.class);
        final List<MatchUrl> actual = parser.parse();

        assertEquals(expected, actual);
    }

    @Test
    public void loadTwoPages() {
        final String firstHtml = readResourceAsString("hltv/loader/results_parser/load_two_pages/firstPage.html");
        final Document firstPage = Jsoup.parse(firstHtml);
        PowerMockito.when(JsoupUtil.getHtmlFromURL(RESULTS_URL))
                .thenReturn(firstPage);

        final String secondPageUrl = "https://www.hltv.org/results?offset=578&startDate=2012-01-01&endDate=2012-12-31";
        final String secondHtml = readResourceAsString("hltv/loader/results_parser/load_two_pages/secondPage.html");
        final Document secondPage = Jsoup.parse(secondHtml);
        PowerMockito.when(JsoupUtil.getHtmlFromURL(secondPageUrl))
                .thenReturn(secondPage);

        final List<MatchUrl> expected = readJsonList("hltv/loader/results_parser/load_two_pages/expected.json", MatchUrl.class);
        final List<MatchUrl> actual = parser.parse();

        assertEquals(expected, actual);

        PowerMockito.verifyStatic(JsoupUtil.class);
        JsoupUtil.getHtmlFromURL(secondPageUrl);
    }
}
