package ru.mskgroup.statandbet.hltv.service.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mskgroup.statandbet.hltv.model.GamesCount;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.dto.TeamMatchDates;
import ru.mskgroup.statandbet.hltv.model.parser.Match;

import java.util.List;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class MatchDaoIT extends AbstractDaoIT {
    @Autowired
    private MatchDao matchDao;

    @Before
    public void checkTablesAreEmpty() {
        assertEquals("Test tables must be empty", 0, matchDao.getMatchUniqueIds().size());
    }

    @Test
    public void crud() {
        final List<Match> expected = readJsonList("hltv/service/dao/crud/expected.json", Match.class);
        matchDao.save(expected);

        final List<Match> actual = matchDao.getAll();
        assertEquals(expected, actual);
    }

    @Test
    public void countDuplicates() {
        final List<Match> matches = readJsonList("hltv/service/dao/duplicates/matches.json", Match.class);
        matchDao.save(matches);

        final int actual = matchDao.countDuplicateMatchIds(asList("1_0", "1337_0"));
        assertEquals(2, actual);
    }

    @Test
    public void getMatchIds() {
        final List<Match> matches = readJsonList("hltv/service/dao/match_ids/matches.json", Match.class);
        matchDao.save(matches);

        final List<String> expected = asList("1_0", "2_0");
        final List<String> actual = matchDao.getMatchUniqueIds();
        assertEquals(expected, actual);
    }

    @Test
    public void getMatchResults() {
        final List<Match> matches = readJsonList("hltv/service/dao/match_results/matches.json", Match.class);
        matchDao.save(matches);

        final List<MatchOutcomeDto> expected = readJsonList("hltv/service/dao/match_results/expected.json", MatchOutcomeDto.class);
        final List<MatchOutcomeDto> actual = matchDao.getMatchOutcomeDtos();
        assertEquals(expected, actual);
    }

    @Test
    public void crudGamesCount() {
        final List<Match> matches = readJsonList("hltv/service/dao/crud_games_count/matches.json", Match.class);
        matchDao.save(matches); // needed for saving teams

        final List<GamesCount> expected = readJsonList("hltv/service/dao/crud_games_count/expected.json", GamesCount.class);
        matchDao.saveGamesCount(expected);

        final List<GamesCount> actual = matchDao.getGamesCount();
        assertEquals(expected, actual);

        // test truncate
        matchDao.truncateGamesCount();
        assertEquals(0, matchDao.getGamesCount().size());
    }

    @Test
    public void getTeamGames() {
        final List<Match> matches = readJsonList("hltv/service/dao/team_games/matches.json", Match.class);
        matchDao.save(matches);

        final List<TeamMatchDates> expected = readJsonList("hltv/service/dao/team_games/expected.json", TeamMatchDates.class);
        final List<TeamMatchDates> actual = matchDao.getTeamGames();
        assertEquals(expected, actual);
    }

    @Test
    public void teamNameMismatch() {
        final List<Match> expected = readJsonList("hltv/service/dao/team_name_mismatch/expected.json", Match.class);
        matchDao.save(expected);

        final List<Match> actual = matchDao.getAll();
        assertEquals(expected, actual);
    }
}
