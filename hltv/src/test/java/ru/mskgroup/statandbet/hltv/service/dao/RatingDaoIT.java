package ru.mskgroup.statandbet.hltv.service.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;

import java.util.List;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class RatingDaoIT extends AbstractDaoIT {
    @Autowired
    private RatingDao ratingDao;

    @Test
    @Sql(scripts = "classpath:sql/insert_teams.sql")
    public void crud() {
        final List<Rating> ratings = readJsonList("hltv/service/dao/rating_crud/ratings.json", Rating.class);
        ratingDao.saveRatings(ratings);

        final List<Rating> actual = ratingDao.getRatings();
        assertEquals(ratings, actual);

        // update
        final List<Rating> updatedRatings = readJsonList("hltv/service/dao/rating_crud/ratings_upd.json", Rating.class);
        ratingDao.saveRatings(updatedRatings);

        final List<Rating> updatedActual = ratingDao.getRatings();
        assertEquals(updatedRatings, updatedActual);
    }
}
