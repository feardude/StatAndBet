package ru.mskgroup.statandbet.hltv.loader;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchParser;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.feardude.commons.ResourceUtils.readJson;
import static com.feardude.commons.ResourceUtils.readJsonList;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
@AllArgsConstructor
@Slf4j
public class MatchParserIT {
    private static final MatchParser MATCH_PARSER = new MatchParser();

    private final String resourceDirName;

    @Parameters(name = "{0}")
    public static Collection<Object> data() {
        return asList(
                "bad_url",
                "bo1",
                "bo3_has_maps_score",
                "bo3_no_maps_score",
                "bo7",
                "mismatch",
                "no_mismatch"
        );
    }

    @Test
    public void parse() {
        final MatchUrl matchUrl = readJson(
                format("hltv/loader/match_parser/%s/match_url.json", resourceDirName),
                MatchUrl.class);
        final List<Match> expected = new ArrayList<>(readJsonList(
                format("hltv/loader/match_parser/%s/expected.json", resourceDirName),
                Match.class
        ));

        final List<Match> actual = MATCH_PARSER.parse(singletonList(matchUrl));
        assertEquals(expected, actual);
    }
}
