package ru.mskgroup.statandbet.hltv.loader;

import org.junit.After;
import org.junit.Test;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchParser;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchPersistence;
import ru.mskgroup.statandbet.hltv.loader.parsing.ResultsParser;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;
import ru.mskgroup.statandbet.hltv.service.HltvService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static com.feardude.commons.ResourceUtils.writeJsonToTestResources;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class HltvLoaderTest {
    private final HltvService hltvService = mock(HltvService.class);
    private final MatchParser matchParser = mock(MatchParser.class);
    private final MatchPersistence matchPersistence = mock(MatchPersistence.class);
    private final ResultsParser resultsParser = mock(ResultsParser.class);
    private final HltvLoader loader = new HltvLoader(hltvService, matchParser, matchPersistence, resultsParser);

    @After
    public void verifyMocks() {
        verifyNoMoreInteractions(hltvService, matchParser, matchPersistence, resultsParser);
    }

    @Test
    public void loadNoNewMatches() {
        final String baseResourcePath = "hltv/loader/hltv_loader/load_no_new_matches/";

        final List<MatchUrl> matchUrls = readJsonList(baseResourcePath + "match_urls.json", MatchUrl.class);
        when(resultsParser.parse())
                .thenReturn(matchUrls);

        final List<String> urls = readJsonList(baseResourcePath + "urls.json", String.class);
        final Set<String> savedMatchesUrls = new HashSet<>(
                readJsonList(baseResourcePath + "saved_matches_urls.json", String.class)
        );
        when(hltvService.getSavedMatchesUrls(urls))
                .thenReturn(savedMatchesUrls);

        loader.load();

        verify(resultsParser).parse();
        verify(hltvService).getSavedMatchesUrls(urls);
    }

    @Test
    public void loadNewMatch() {
        final String baseResourcePath = "hltv/loader/hltv_loader/load_new_match/";

        final List<MatchUrl> matchUrls = readJsonList(baseResourcePath + "match_urls.json", MatchUrl.class);
        when(resultsParser.parse())
                .thenReturn(matchUrls);

        final List<String> urls = readJsonList(baseResourcePath + "urls.json", String.class);
        final Set<String> savedMatchesUrls = new HashSet<>(
                readJsonList(baseResourcePath + "saved_matches_urls.json", String.class)
        );
        when(hltvService.getSavedMatchesUrls(urls))
                .thenReturn(savedMatchesUrls);

        final List<Match> matches = singletonList(Match.builder().build());
        when(matchParser.parse(matchUrls))
                .thenReturn(matches);

        loader.load();

        verify(resultsParser).parse();
        verify(hltvService).getSavedMatchesUrls(urls);
        verify(matchParser).parse(matchUrls);
        verify(matchPersistence).save(matches);
        verify(hltvService).calcGamesCounts();
    }
}
