package ru.mskgroup.statandbet.hltv.calculator;

import org.junit.BeforeClass;
import org.junit.Test;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static com.feardude.commons.ResourceUtils.readJsonMap;
import static java.util.Comparator.comparing;
import static org.junit.Assert.assertEquals;

public class RatingCalculatorTest {
    private static RatingCalculator ratingCalculator;

    @BeforeClass
    public static void setUp() {
        final Map<String, Map<LocalDateTime, Integer>> gamesCountByDatetimeAndTeam = new HashMap<>(3);

        final Map<LocalDateTime, Integer> team1GamesCount = readJsonMap("hltv/calculator/team_1_games_count.json",
                LocalDateTime.class, Integer.class);
        gamesCountByDatetimeAndTeam.put("team1", team1GamesCount);

        final Map<LocalDateTime, Integer> team2GamesCount = readJsonMap("hltv/calculator/team_2_games_count.json",
                LocalDateTime.class, Integer.class);
        gamesCountByDatetimeAndTeam.put("team2", team2GamesCount);

        final Map<LocalDateTime, Integer> team3GamesCount = readJsonMap("hltv/calculator/team_3_games_count.json",
                LocalDateTime.class, Integer.class);
        gamesCountByDatetimeAndTeam.put("team3", team3GamesCount);

        ratingCalculator = new RatingCalculator();
        ratingCalculator.setGamesCountByDatetimeAndTeam(gamesCountByDatetimeAndTeam);
    }

    @Test
    public void updateRatings_2teams() {
        final List<Rating> oldRatings = readJsonList("hltv/calculator/2_teams/old_ratings.json", Rating.class);
        final List<MatchOutcomeDto> matchResults = readJsonList("hltv/calculator/2_teams/match_results.json",
                MatchOutcomeDto.class);

        final List<Rating> expectedNewRatings = readJsonList("hltv/calculator/2_teams/expected.json", Rating.class);
        final List<Rating> actualNewRatings = ratingCalculator.calculateRatings(oldRatings, matchResults);

        assertEquals(expectedNewRatings, actualNewRatings);
    }

    @Test
    public void updateRatings_3teams() {
        final List<Rating> oldRatings = readJsonList("hltv/calculator/3_teams/old_ratings.json", Rating.class);
        final List<MatchOutcomeDto> matchResults = readJsonList("hltv/calculator/3_teams/match_results.json",
                MatchOutcomeDto.class);

        final List<Rating> expectedNewRatings = readJsonList("hltv/calculator/3_teams/expected.json", Rating.class);
        final List<Rating> actualNewRatings = ratingCalculator.calculateRatings(oldRatings, matchResults);
        actualNewRatings.sort(comparing(Rating::getTeamName));

        assertEquals(expectedNewRatings, actualNewRatings);
    }
}
