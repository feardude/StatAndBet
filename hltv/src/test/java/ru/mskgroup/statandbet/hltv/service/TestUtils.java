package ru.mskgroup.statandbet.hltv.service;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertTrue;

public class TestUtils {
    private static final String TEST_RESOURCE_PATH = "src/test/resources/";

    private TestUtils() {}

    public static void checkTestResourceFileExists(String filePath) {
        final File testResourceFile = getTestResourceFile(filePath);
        final String message = String.format("Test resource file '%s' not found", filePath);
        assertTrue(message, testResourceFile.exists());
    }

    public static List<String> readLines(String filePath) {
        final File testResourceFile = getTestResourceFile(filePath);
        try {
            return FileUtils.readLines(testResourceFile, UTF_8);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private static File getTestResourceFile(String filePath) {
        return new File(TEST_RESOURCE_PATH + filePath);
    }
}
