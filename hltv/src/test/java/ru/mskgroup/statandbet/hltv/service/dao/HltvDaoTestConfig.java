package ru.mskgroup.statandbet.hltv.service.dao;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import ru.mskgroup.statandbet.hltv.config.HltvServiceConfig;

@Configuration
@Import(HltvServiceConfig.class)
public class HltvDaoTestConfig {
    @Bean
    public PlatformTransactionManager platformTransactionManager(BasicDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
