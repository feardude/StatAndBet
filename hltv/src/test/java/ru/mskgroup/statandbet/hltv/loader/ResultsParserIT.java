package ru.mskgroup.statandbet.hltv.loader;

import org.jsoup.nodes.Document;
import org.junit.Ignore;
import org.junit.Test;
import ru.mskgroup.commons.JsoupUtil;
import ru.mskgroup.statandbet.hltv.loader.parsing.ResultsParser;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;

import java.util.List;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ResultsParserIT {
    private static final String resultsUrlPattern = "/results?startDate=%d-01-01&endDate=%d-12-31";
    private ResultsParser parser = new ResultsParser(resultsUrlPattern, null);

    @Test
    @Ignore // somehow hltv.org fixed this page
    public void error500_returnEmptyList() {
        final String url = "https://www.hltv.org/results?offset=2900&startDate=2014-01-01&endDate=2014-12-31";
        final Document html = JsoupUtil.getHtmlFromURL(url);
        assertEquals(0, parser.getMatchUrls(html).size());
        assertEquals("/results?offset=3000&startDate=2014-01-01&endDate=2014-12-31", parser.getNextResultsPageUrl(html, url));
    }

    @Test
    public void checkUrlCountAndThreeRandomUrls() {
        final List<MatchUrl> expected = readJsonList("hltv/loader/results_parser/parse_all/expected.json", MatchUrl.class);

        final Document html = JsoupUtil.getHtmlFromURL("https://www.hltv.org/results?startDate=2015-01-01&endDate=2015-12-31");
        final List<MatchUrl> actual = parser.getMatchUrls(html);

        assertEquals(100, actual.size());
        assertTrue("MatchUrl list does not contain all of expected match urls", actual.containsAll(expected));
    }

    @Test
    public void filterDuplicateTeamNames() {
        final List<MatchUrl> expected = readJsonList("hltv/loader/results_parser/filter_team_names/expected.json", MatchUrl.class);

        final Document html = JsoupUtil.getHtmlFromURL("https://www.hltv.org/results?startDate=2016-05-01&endDate=2016-05-31");
        final List<MatchUrl> actual = parser.getMatchUrls(html);

        assertEquals(99, actual.size());
        assertTrue("MatchUrl list does not contain all of expected match urls", actual.containsAll(expected));
    }
}
