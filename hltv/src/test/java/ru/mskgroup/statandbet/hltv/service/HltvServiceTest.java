package ru.mskgroup.statandbet.hltv.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import ru.mskgroup.statandbet.hltv.model.GamesCount;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;
import ru.mskgroup.statandbet.hltv.model.dto.TeamMatchDates;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.service.dao.MatchDao;
import ru.mskgroup.statandbet.hltv.service.dao.RatingDao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.feardude.commons.ResourceUtils.readJsonList;
import static com.feardude.commons.ResourceUtils.readJsonMapValuesMap;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@Slf4j
public class HltvServiceTest {
    private final MatchDao matchDao = mock(MatchDao.class);
    private final RatingDao ratingDao = mock(RatingDao.class);
    private final HltvService hltvService = new HltvServiceImpl(matchDao, ratingDao);

    @Test
    public void calcGameCounts() {
        final List<TeamMatchDates> teamMatchDates = readJsonList("hltv/service/eval_games_count/team_games.json",
                TeamMatchDates.class);
        when(matchDao.getTeamGames())
                .thenReturn(teamMatchDates);

        hltvService.calcGamesCounts();

        final List<GamesCount> expected = readJsonList("hltv/service/eval_games_count/expected.json", GamesCount.class);
        verify(matchDao).truncateGamesCount();
        verify(matchDao).getTeamGames();
        verify(matchDao).saveGamesCount(expected);
        verifyNoMoreInteractions(matchDao);
    }

    @Test
    public void getGamesCountMap() {
        final List<GamesCount> gamesCount = readJsonList("hltv/service/games_count_map/games_count.json", GamesCount.class);
        when(matchDao.getGamesCount())
                .thenReturn(gamesCount);

        Map<String, Map<LocalDateTime, Integer>> expected = readJsonMapValuesMap(
                "hltv/service/games_count_map/expected.json",
                String.class, LocalDateTime.class, Integer.class
        );

        Map<String, Map<LocalDateTime, Integer>> actual = hltvService.getGamesCounts();
        assertReflectionEquals("Games count mapping is not equal", expected, actual);
    }

    @Test
    public void getMatchUniqueIds() {
        hltvService.getMatchUniqueIds();
        verify(matchDao).getMatchUniqueIds();
    }

    @Test
    public void getMatchOutcomeDtos() {
        hltvService.getMatchOutcomeDtos();
        verify(matchDao).getMatchOutcomeDtos();
    }

    @Test
    public void getRatings() {
        hltvService.getRatings();
        verify(ratingDao).getRatings();
    }

    @Test
    public void getSavedMatchesUrls() {
        final List<String> urls = singletonList("url");
        hltvService.getSavedMatchesUrls(urls);
        verify(matchDao).getSavedMatchesUrls(urls);
    }

    @Test
    public void countDuplicateMatchIds() {
        final List<String> matchIds = singletonList("id");
        hltvService.countDuplicateMatchIds(matchIds);
        verify(matchDao).countDuplicateMatchIds(matchIds);
    }

    @Test
    public void saveMatches() {
        final List<Match> matches = singletonList(Match.builder().build());
        hltvService.saveMatches(matches);
        verify(matchDao).save(matches);
    }

    @Test
    public void saveRatings() {
        final List<Rating> ratings = singletonList(Rating.builder().build());
        hltvService.saveRatings(ratings);
        verify(ratingDao).saveRatings(ratings);
    }
}
