package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class PrizePool {
    private final Type type;
    private final String value;

    private enum Type {
        CASH, TICKET
    }
}
