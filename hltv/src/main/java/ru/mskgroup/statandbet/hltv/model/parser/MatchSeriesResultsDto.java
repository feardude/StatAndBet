package ru.mskgroup.statandbet.hltv.model.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class MatchSeriesResultsDto {
    private final int ordinal;
    private final String mapName;
    private final MatchResult team1Result;
    private final MatchResult team2Result;
}
