package ru.mskgroup.statandbet.hltv.loader;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import ru.mskgroup.statandbet.hltv.config.HltvLoaderConfig;

@Import(HltvLoaderConfig.class)
@SpringBootApplication
@AllArgsConstructor
@Slf4j
public class HltvLoaderStarter {
    private final HltvLoader hltvLoader;

    public static void main(String[] args) {
        try {
            SpringApplication.run(HltvLoaderStarter.class);
            System.exit(0);
        } catch (Exception e) {
            log.error("HLTV loader failed.", e);
        }
    }

    @Bean
    public CommandLineRunner run() {
        return args -> hltvLoader.load();
    }
}
