package ru.mskgroup.statandbet.hltv.service.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections4.ListUtils;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.mskgroup.statandbet.hltv.model.GamesCount;
import ru.mskgroup.statandbet.hltv.model.MatchOutcome;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto.TeamOutcome;
import ru.mskgroup.statandbet.hltv.model.dto.MatchResultDto;
import ru.mskgroup.statandbet.hltv.model.dto.TeamMatchDates;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.model.parser.Match.MatchBuilder;
import ru.mskgroup.statandbet.hltv.model.parser.MatchResult;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Stream.concat;

@Repository
@AllArgsConstructor
@Slf4j
public class JdbcMatchDao implements MatchDao {
    private static final int MAX_BATCH_SIZE = 1000;

    private static final String SAVE_EVENT = "INSERT INTO EVENT (NAME) VALUES (:name) " +
            "ON CONFLICT DO NOTHING";

    private static final String SAVE_MAP = "INSERT INTO MAP (NAME) VALUES (:name) " +
            "ON CONFLICT DO NOTHING";

    private static final String SAVE_TEAM = "INSERT INTO TEAM (NAME) VALUES (:name) " +
            "ON CONFLICT DO NOTHING";

    private static final String SAVE_MATCH_RESULT =
            "INSERT INTO match_result (match_id, team_id, score, outcome) " +
                    "VALUES (:matchId, :teamId, :score, :outcome)";

    private static final String SAVE_MATCH =
            "INSERT INTO match (hltv_match_id, ordinal, unique_id, date_time, map_id, event_id, format, url, team_name_mismatch) " +
                    "VALUES (:hltvMatchId, :ordinal, :uniqueId, :dateTime, :mapId, :eventId, :format, :url, :teamNameMismatch)";

    private static final String SAVE_GAMES_COUNT =
            "INSERT INTO games_count (team_id, valid_on, count) " +
                    "VALUES ( " +
                    "  (SELECT team.id " +
                    "   FROM team " +
                    "   WHERE team.name = :teamName), " +
                    "  :validOn, :count)";

    private static final String GET_ALL_MATCHES =
            "SELECT match.id, hltv_match_id, date_time, ordinal, format, url, " +
                    "team_name_mismatch, m.name AS map, e.name AS event " +
                    "FROM match " +
                    "  JOIN map m ON match.map_id = m.id " +
                    "  JOIN event e ON match.event_id = e.id";

    private static final String GET_MATCH_RESULTS =
            "SELECT t.name, score, outcome " +
                    "FROM match_result mr " +
                    "JOIN team t ON mr.team_id = t.id " +
                    "WHERE mr.match_id = :matchId";

    private static final String GET_EVENTS = "SELECT ID, NAME FROM EVENT";

    private static final String GET_MAPS = "SELECT ID, NAME FROM MAP";

    private static final String GET_TEAMS = "SELECT ID, NAME FROM TEAM";

    private static final String GET_MATCH_UNIQUE_IDS = "SELECT unique_id FROM MATCH";

    private static final String COUNT_UNIQUE_IDS =
            "SELECT COUNT(unique_id) AS matches " +
                    "FROM match " +
                    "WHERE unique_id IN (:uniqueIds)";

    private static final String GET_MATCH_OUTCOME_DTO =
            "SELECT " +
                    "  t1.name AS team_1," +
                    "  mr.outcome AS outcome_1," +
                    "  t2.name AS team_2," +
                    "  mr2.outcome AS outcome_2," +
                    "  m.date_time " +
                    "FROM match_result mr " +
                    "  JOIN match_result mr2 ON mr.match_id = mr2.match_id " +
                    "  JOIN team t1 ON mr.team_id = t1.id " +
                    "  JOIN team t2 ON mr2.team_id = t2.id " +
                    "  JOIN match m ON mr.match_id = m.id " +
                    "WHERE mr.team_id <> mr2.team_id AND mr.id < mr2.id " +
                    "ORDER BY m.date_time ASC";

    private static final String GET_TEAM_GAMES =
            "SELECT t.name, m.date_time " +
                    "FROM match_result mr " +
                    "  JOIN team t ON mr.team_id = t.id " +
                    "  JOIN match m ON mr.match_id = m.id";

    private static final String DELETE_MATCH =
            "DELETE FROM match " +
                    "WHERE hltv_match_id IN (:hltvMatchIds)";

    private static final String GET_GAMES_COUNT =
            "SELECT t.name, gc.valid_on, gc.count " +
                    "FROM games_count gc " +
                    "  JOIN team t ON gc.team_id = t.id";

    private static final String TRUNCATE_GAMES_COUNT =
            "TRUNCATE TABLE games_count " +
                    "RESTART IDENTITY " +
                    "RESTRICT";

    private static final String GET_MATCH_URLS =
            "select distinct url " +
                    "from match " +
                    "where url in (:urls)";

    private final NamedParameterJdbcOperations jdbcTemplate;

    @Override
    public List<Match> getAll() {
        return jdbcTemplate.query(GET_ALL_MATCHES, toMatchQueryResponse()).stream()
                .map(matchQueryResponse -> {
                    List<MatchResult> matchResults = jdbcTemplate.query(GET_MATCH_RESULTS,
                            new MapSqlParameterSource("matchId", matchQueryResponse.getId()),
                            toMatchResult());
                    return matchQueryResponse.getMatchBuilder()
                            .team1(matchResults.get(0))
                            .team2(matchResults.get(1))
                            .build();
                })
                .collect(toList());
    }

    @Override
    public void save(List<Match> matches) {
        Map<String, Integer> mapNameToMapId = saveMapping(
                mapMatchToString(matches, Match::getMap),
                SAVE_MAP, GET_MAPS
        );
        Map<String, Integer> eventToEventId = saveMapping(
                mapMatchToString(matches, Match::getEvent),
                SAVE_EVENT, GET_EVENTS
        );
        Map<String, Integer> teamToTeamId = saveMapping(
                concat(
                        mapMatchToString(matches, match -> match.getTeam1().getTeamName()),
                        mapMatchToString(matches, match -> match.getTeam2().getTeamName())
                ),
                SAVE_TEAM, GET_TEAMS
        );
        matches.forEach(
                match -> {
                    KeyHolder keyHolder = new GeneratedKeyHolder();
                    try {
                        jdbcTemplate.update(SAVE_MATCH,
                                saveMatchParams(match,
                                        mapNameToMapId.get(match.getMap()),
                                        eventToEventId.get(match.getEvent())
                                ),
                                keyHolder, new String[]{"id"}
                        );
                    } catch (DuplicateKeyException e) {
                        log.error("Duplicate key while trying to save match=[{}]", match, e);
                    }
                    saveMatchResults(keyHolder.getKey().intValue(), match, teamToTeamId);
                }
        );
    }

    @Override
    public List<String> getMatchUniqueIds() {
        return jdbcTemplate.query(GET_MATCH_UNIQUE_IDS, (rs, rowNum) -> rs.getString("unique_id"));
    }

    @Override
    public int countDuplicateMatchIds(List<String> uniqueIds) {
        return ListUtils.partition(uniqueIds, MAX_BATCH_SIZE).stream()
                .map(this::doCountDuplicateMatchIds)
                .reduce(Integer::sum)
                .orElse(0);
    }

    private int doCountDuplicateMatchIds(List<String> uniqueIds) {
        return jdbcTemplate.queryForObject(
                COUNT_UNIQUE_IDS,
                new MapSqlParameterSource("uniqueIds", uniqueIds),
                Integer.class
        );
    }

    @Override
    public List<MatchOutcomeDto> getMatchOutcomeDtos() {
        return jdbcTemplate.query(GET_MATCH_OUTCOME_DTO, toMatchOutcomeDto());
    }

    @Override
    public void saveGamesCount(List<GamesCount> gamesCounts) {
        SqlParameterSource[] batchArgs = gamesCounts.stream()
                .map(this::saveGamesCountParams)
                .toArray(SqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(SAVE_GAMES_COUNT, batchArgs);
    }

    @Override
    public List<GamesCount> getGamesCount() {
        return jdbcTemplate.query(GET_GAMES_COUNT, toGamesCount());
    }

    @Override
    public List<TeamMatchDates> getTeamGames() {
        return jdbcTemplate.query(GET_TEAM_GAMES, toTeamGameDate())
                .stream()
                .collect(toMap(
                        TeamGameDate::getTeamName,
                        dto -> singletonList(dto.getDateTime()),
                        (list1, list2) -> Stream.concat(list1.stream(), list2.stream())
                                .sorted()
                                .collect(toList())
                )).entrySet().stream()
                .map(entry -> TeamMatchDates.builder()
                        .teamName(entry.getKey())
                        .matchDates(entry.getValue())
                        .build())
                .collect(toList());
    }

    @Override
    public void deleteMatches(List<Integer> hltvMatchIds) {
        jdbcTemplate.update(DELETE_MATCH, new MapSqlParameterSource("hltvMatchIds", hltvMatchIds));
    }

    @Override
    public void truncateGamesCount() {
        jdbcTemplate.update(TRUNCATE_GAMES_COUNT, emptyMap());
    }

    @Override
    public Set<String> getSavedMatchesUrls(List<String> urls) {
        return ListUtils.partition(urls, MAX_BATCH_SIZE).stream()
                .map(this::doGetSavedMatchesUrls)
                .flatMap(Collection::stream)
                .collect(toSet());
    }

    private Set<String> doGetSavedMatchesUrls(List<String> urls) {
        return new HashSet<>(
                jdbcTemplate.query(
                        GET_MATCH_URLS,
                        new MapSqlParameterSource("urls", urls),
                        (rs, rowNum) -> rs.getString("url")
                )
        );
    }

    private Map<String, Integer> saveMapping(Stream<String> objectStream, String saveSql, String getSql) {
        final List<String> objects = objectStream
                .distinct()
                .sorted()
                .collect(toList());
        final SqlParameterSource[] batchArgs = objects.stream()
                .map(objectName -> new MapSqlParameterSource("name", objectName))
                .toArray(SqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(saveSql, batchArgs);

        return jdbcTemplate.query(getSql, nameToIdMapping())
                .stream()
                .flatMap(mapping -> mapping.entrySet().stream())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Stream<String> mapMatchToString(List<Match> matches, Function<Match, String> mapper) {
        return matches.stream().map(mapper);
    }

    private void saveMatchResults(int matchId, Match match, Map<String, Integer> teamsMapping) {
        val matchResultDto1 = buildMatchResultDto(match.getTeam1(), teamsMapping);
        val matchResultDto2 = buildMatchResultDto(match.getTeam2(), teamsMapping);
        val batchArgs = Stream.of(matchResultDto1, matchResultDto2)
                .map(dto -> saveMatchResultParams(matchId, dto))
                .toArray(SqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(SAVE_MATCH_RESULT, batchArgs);
    }

    private MatchResultDto buildMatchResultDto(MatchResult matchResult, Map<String, Integer> teamsMapping) {
        return MatchResultDto.builder()
                .teamId(teamsMapping.get(matchResult.getTeamName()))
                .score(matchResult.getScore())
                .outcome(matchResult.getOutcome())
                .build();
    }


    private SqlParameterSource saveMatchParams(Match match, int mapId, int eventId) {
        return new MapSqlParameterSource()
                .addValue("hltvMatchId", match.getHltvMatchId())
                .addValue("ordinal", match.getOrdinal())
                .addValue("uniqueId", match.buildUniqueId())
                .addValue("dateTime", Timestamp.valueOf(match.getDateTime()))
                .addValue("mapId", mapId)
                .addValue("eventId", eventId)
                .addValue("format", match.getFormat())
                .addValue("url", match.getUrl())
                .addValue("teamNameMismatch", match.isTeamNameMismatch());
    }

    private SqlParameterSource saveMatchResultParams(int matchId, MatchResultDto dto) {
        return new MapSqlParameterSource()
                .addValue("matchId", matchId)
                .addValue("teamId", dto.getTeamId())
                .addValue("score", dto.getScore())
                .addValue("outcome", dto.getOutcome().toString());
    }

    private SqlParameterSource saveGamesCountParams(GamesCount gamesCount) {
        return new MapSqlParameterSource()
                .addValue("teamName", gamesCount.getTeamName())
                .addValue("validOn", gamesCount.getValidOn())
                .addValue("count", gamesCount.getCount());
    }


    private RowMapper<Map<String, Integer>> nameToIdMapping() {
        return (rs, rowNum) -> {
            val mapping = new HashMap<String, Integer>(1);
            mapping.put(rs.getString("name"), rs.getInt("id"));
            return mapping;
        };
    }

    private RowMapper<MatchQueryResponse> toMatchQueryResponse() {
        return (rs, rowNum) -> MatchQueryResponse.builder()
                .id(rs.getInt("id"))
                .matchBuilder(Match.builder()
                        .hltvMatchId(rs.getInt("hltv_match_id"))
                        .dateTime(rs.getTimestamp("date_time").toLocalDateTime())
                        .ordinal(rs.getInt("ordinal"))
                        .map(rs.getString("map"))
                        .event(rs.getString("event"))
                        .format(rs.getString("format"))
                        .url(rs.getString("url"))
                        .teamNameMismatch(rs.getBoolean("team_name_mismatch"))
                )
                .build();
    }

    private RowMapper<MatchResult> toMatchResult() {
        return (rs, rowNum) -> MatchResult.builder()
                .teamName(rs.getString("name"))
                .score(rs.getInt("score"))
                .outcome(toMatchOutcome(rs.getString("outcome")))
                .build();
    }

    private RowMapper<MatchOutcomeDto> toMatchOutcomeDto() {
        return (rs, rowNum) -> MatchOutcomeDto.builder()
                .matchDateTime(rs.getTimestamp("date_time").toLocalDateTime())
                .teamOutcome1(TeamOutcome.builder()
                        .name(rs.getString("team_1"))
                        .outcome(toMatchOutcome(rs.getString("outcome_1"))).build())
                .teamOutcome2(TeamOutcome.builder()
                        .name(rs.getString("team_2"))
                        .outcome(toMatchOutcome(rs.getString("outcome_2"))).build())
                .build();
    }

    private MatchOutcome toMatchOutcome(String s) {
        return MatchOutcome.valueOf(s);
    }

    private RowMapper<GamesCount> toGamesCount() {
        return (rs, rowNum) -> GamesCount.builder()
                .teamName(rs.getString("name"))
                .validOn(rs.getTimestamp("valid_on").toLocalDateTime())
                .count(rs.getInt("count"))
                .build();
    }

    private RowMapper<TeamGameDate> toTeamGameDate() {
        return (rs, rowNum) -> TeamGameDate.builder()
                .teamName(rs.getString("name"))
                .dateTime(rs.getTimestamp("date_time").toLocalDateTime())
                .build();
    }

    @AllArgsConstructor
    @Builder
    @Getter
    private static class MatchQueryResponse {
        private final int id;
        private final MatchBuilder matchBuilder;
    }

    @Builder
    @Getter
    private static class TeamGameDate {
        private final String teamName;
        private final LocalDateTime dateTime;
    }
}
