package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MatchOutcome {
    WIN(1.0d),
    DRAW(0.5d),
    LOSE(0.0d);

    private final double factor;
}
