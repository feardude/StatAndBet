package ru.mskgroup.statandbet.hltv.loader.parsing;

import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import ru.mskgroup.commons.JsoupUtil;
import ru.mskgroup.statandbet.hltv.model.MatchOutcome;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.model.parser.MatchResult;
import ru.mskgroup.statandbet.hltv.model.parser.MatchSeriesResultsDto;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.lang.Integer.parseInt;
import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static ru.mskgroup.commons.DateUtils.unixToLocalDateTime;
import static ru.mskgroup.statandbet.hltv.loader.parsing.ParserUtil.pageLoadedWithError;
import static ru.mskgroup.statandbet.hltv.model.MatchOutcome.DRAW;
import static ru.mskgroup.statandbet.hltv.model.MatchOutcome.LOSE;
import static ru.mskgroup.statandbet.hltv.model.MatchOutcome.WIN;

@Component
@Slf4j
public class MatchParser {
    private static final String HLTV_URL = "https://www.hltv.org";

    public List<Match> parse(List<MatchUrl> matchUrls) {
        return matchUrls.parallelStream()
                .map(this::doParse)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .sorted(comparing(Match::getDateTime))
                .collect(toList());
    }

    public List<Match> doParse(MatchUrl matchUrl) {
        final Document html = JsoupUtil.getHtmlFromURL("https://www.hltv.org" + matchUrl.getUrl());

        if (isNull(html) || pageLoadedWithError(html)) {
            log.warn("Page matchUrl=[{}] was loaded with error. Skipping this match.", matchUrl.getUrl());
            return null;
        }

        final LocalDateTime dateTime = parseDateTime(html);
        final int hltvMatchId = parseHltvMatchId(matchUrl.getUrl());
        final List<String> teams = parseTeams(html);
        final String team1 = teams.get(0);
        final String team2 = teams.get(1);
        final String event = parseEvent(html);
        final String format = parseFormat(html);

        return parseMatchResults(html, team1, team2).stream()
                .map(dto -> Match.builder()
                        .hltvMatchId(hltvMatchId)
                        .ordinal(dto.getOrdinal())
                        .map(dto.getMapName())
                        .team1(dto.getTeam1Result())
                        .team2(dto.getTeam2Result())
                        .dateTime(dateTime.plusMinutes(dto.getOrdinal()))
                        .event(event)
                        .format(format)
                        .url(HLTV_URL + matchUrl.getUrl())
                        .teamNameMismatch(getTeamNameMismatch(matchUrl, team1, team2))
                        .build()
                )
                .collect(toList());
    }

    private LocalDateTime parseDateTime(Document html) {
        final String dateTimeStartUnix = html.getElementsByClass("timeAndEvent").first()
                .getElementsByClass("time").first()
                .attr("data-unix");
        return unixToLocalDateTime(dateTimeStartUnix);
    }

    private Integer parseHltvMatchId(String url) {
        //    / matches/2290279/%event_name%
        // [0] [1]     [2]      [3]
        return parseInt(url.split("/")[2]);
    }

    private List<String> parseTeams(Document html) {
        return html.getElementsByClass("teamsBox")
                .first()
                .getElementsByClass("team")
                .stream()
                .map(team -> team.getElementsByClass("teamName").first())
                .map(Element::text)
                .collect(toList());
    }

    private String parseEvent(Document html) {
        return html.getElementsByClass("timeAndEvent").first()
                .getElementsByClass("event").first()
                .getElementsByTag("a").first()
                .attr("title");
    }

    private String parseFormat(Document html) {
        int totalMaps = html.getElementsByClass("mapHolder").size();
        if (totalMaps == 2 || totalMaps == 4) {
            totalMaps++;
        }
        return String.format("bo%d", totalMaps);
    }

    private List<MatchSeriesResultsDto> parseMatchResults(Document html, String team1, String team2) {
        final List<MatchSeriesResultsDto> matchResults = new ArrayList<>();

        final int matchResultsCount = html.getElementsByClass("results").size();
        if (matchResultsCount == 0) {
            int score1 = parseScoreFromTeamInfo(html, 1);
            int score2 = parseScoreFromTeamInfo(html, 2);
            return singletonList(buildMatchResultsForMap(0, "", team1, score1, team2, score2));
        }

        int ordinal = 0;
        for (Element map : html.getElementsByClass("mapholder")) {
            Elements results = map.getElementsByClass("results");
            if (!results.isEmpty()) {
                String mapName = map.getElementsByClass("mapname").first().text();
                matchResults.add(parseMatchResult(ordinal++, mapName, results, team1, team2));
            }
        }
        return matchResults;
    }

    private MatchSeriesResultsDto parseMatchResult(int ordinal, String mapName, Elements results,
                                                   String team1, String team2) {
        final Elements resultScoreSpans = results.first().getElementsByTag("span");
        final int score1 = parseInt(resultScoreSpans.get(0).text());
        final int score2 = parseInt(resultScoreSpans.get(2).text());
        return buildMatchResultsForMap(ordinal, mapName, team1, score1, team2, score2);
    }

    private MatchSeriesResultsDto buildMatchResultsForMap(int ordinal, String mapName, String team1, int score1,
                                                          String team2, int score2) {
        return MatchSeriesResultsDto.builder()
                .ordinal(ordinal)
                .mapName(mapName)
                .team1Result(
                        MatchResult.builder()
                                .teamName(team1)
                                .score(score1)
                                .outcome(calcOutcome(score1, score2))
                                .build())
                .team2Result(
                        MatchResult.builder()
                                .teamName(team2)
                                .score(score2)
                                .outcome(calcOutcome(score2, score1))
                                .build())
                .build();
    }

    private Integer parseScoreFromTeamInfo(Document html, int teamNumber) {
        return html.getElementsByClass("teamsBox").first()
                .getElementsByClass(String.format("team%d-gradient", teamNumber)).first()
                .children().stream()
                .filter(e -> e.tagName().equals("div"))
                .findFirst()
                .map(element -> parseInt(element.text()))
                .orElse(0);
    }

    private MatchOutcome calcOutcome(int score1, int score2) {
        if (score1 == score2) {
            return DRAW;
        }
        return score1 > score2 ? WIN : LOSE;
    }

    private boolean getTeamNameMismatch(MatchUrl matchUrl, String parsedTeam1, String parsedTeam2) {
        return (!parsedTeam1.equalsIgnoreCase(matchUrl.getTeam1()) ||
                !parsedTeam2.equalsIgnoreCase(matchUrl.getTeam2())
        ) && !(parsedTeam1.equalsIgnoreCase(matchUrl.getTeam2()) &&
                parsedTeam2.equalsIgnoreCase(matchUrl.getTeam1())
        );
    }
}
