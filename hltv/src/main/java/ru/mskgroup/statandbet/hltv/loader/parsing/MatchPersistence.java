package ru.mskgroup.statandbet.hltv.loader.parsing;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.service.HltvService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
@AllArgsConstructor
@Slf4j
public class MatchPersistence {
    private final HltvService hltvService;

    public void save(List<Match> matches) {
        final List<Match> updatedMatches = updateDateTime(matches);

        final int duplicates = countDuplicates(updatedMatches);
        if (duplicates > 0) {
            log.info("Found {} duplicate matches", duplicates);
            final List<Match> uniqueMatches = retainUniqueMatches(updatedMatches);
            if (!uniqueMatches.isEmpty()) {
                log.info("Saving {} unique matches", uniqueMatches.size());
                hltvService.saveMatches(uniqueMatches);
            }
            return;
        }

        hltvService.saveMatches(updatedMatches);
    }

    private int countDuplicates(List<Match> newMatches) {
        return hltvService.countDuplicateMatchIds(
                newMatches.stream()
                        .map(Match::buildUniqueId)
                        .collect(toList())
        );
    }

    private List<Match> retainUniqueMatches(List<Match> newMatches) {
        final List<String> oldMatchIds = hltvService.getMatchUniqueIds();
        return newMatches.stream()
                .filter(m -> !oldMatchIds.contains(m.buildUniqueId()))
                .collect(toList());

    }

    private List<Match> updateDateTime(List<Match> matches) {
        final Map<String, Match> uniqueIdToMatch = matches.stream()
                .collect(toMap(
                        Match::buildUniqueId,
                        Function.identity()
                ));

        final long uniqueDatesCount = matches.stream()
                .map(Match::getDateTime)
                .distinct()
                .count();

        if (uniqueDatesCount != matches.size()) {
            final Map<String, List<String>> teamNameToMatches = teamNameToMatchUniqueId(matches);
            for (Map.Entry<String, List<String>> teamNameToMatchUniqueIds : teamNameToMatches.entrySet()) {
                final LinkedList<Match> teamMatches = teamNameToMatchUniqueIds.getValue().stream()
                        .map(uniqueIdToMatch::get)
                        .sorted(comparing(Match::getDateTime))
                        .collect(toCollection(LinkedList::new));

                for (int i = 1; i < teamMatches.size(); i++) {
                    final Match prevMatch = teamMatches.get(i - 1);
                    final Match nextMatch = teamMatches.get(i);

                    if (sameDatetimes(prevMatch, nextMatch)) {
                        final String nextMatchUniqueId = nextMatch.buildUniqueId();
                        log.warn("Found matches with clashing datetime [prev-hltv-id={}, next-hltv-id={}, date-time={}]. " +
                                        "Updating for uniqueId={}",
                                prevMatch.getHltvMatchId(), nextMatch.getHltvMatchId(), nextMatch.getDateTime(),
                                nextMatchUniqueId);

                        final Match updatedMatch = nextMatch.toBuilder()
                                .dateTime(prevMatch.getDateTime().plusMinutes(5))
                                .build();
                        uniqueIdToMatch.put(updatedMatch.buildUniqueId(), updatedMatch);
                    }
                }
            }
        }
        return new ArrayList<>(uniqueIdToMatch.values());
    }

    private Map<String, List<String>> teamNameToMatchUniqueId(List<Match> matches) {
        return matches.stream()
                .map(this::teamNameToMatchUniqueId)
                .flatMap(map -> map.entrySet().stream())
                .collect(toMap(
                        Map.Entry::getKey,
                        entry -> singletonList(entry.getValue()),
                        (list1, list2) -> Stream.concat(list1.stream(), list2.stream())
                                .collect(toList())
                ));
    }

    private Map<String, String> teamNameToMatchUniqueId(Match match) {
        final Map<String, String> result = new HashMap<>(2);
        result.put(match.getTeam1().getTeamName(), match.buildUniqueId());
        result.put(match.getTeam2().getTeamName(), match.buildUniqueId());
        return result;
    }

    private boolean sameDatetimes(Match prevMatch, Match nextMatch) {
        return prevMatch.getDateTime().equals(nextMatch.getDateTime());
    }
}
