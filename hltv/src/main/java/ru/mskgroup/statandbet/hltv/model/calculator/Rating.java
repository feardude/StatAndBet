package ru.mskgroup.statandbet.hltv.model.calculator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class Rating {
    private final String teamName;
    private final double value;
}
