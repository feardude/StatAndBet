package ru.mskgroup.statandbet.hltv.model.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder(toBuilder = true)
@Getter
@ToString
@EqualsAndHashCode
public class Match {
    private final int hltvMatchId;
    private final int ordinal;
    private final MatchResult team1;
    private final MatchResult team2;
    private final LocalDateTime dateTime;
    private final String map;
    private final String event;
    private final String format;
    private final String url;
    private final boolean teamNameMismatch;

    public String buildUniqueId() {
        return String.format("%d_%d", hltvMatchId, ordinal);
    }
}
