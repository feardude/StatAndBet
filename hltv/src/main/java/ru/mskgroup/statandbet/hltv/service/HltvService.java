package ru.mskgroup.statandbet.hltv.service;

import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;
import ru.mskgroup.statandbet.hltv.model.parser.Match;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface HltvService {
    List<Rating> getRatings();

    void saveRatings(List<Rating> ratings);

    List<MatchOutcomeDto> getMatchOutcomeDtos();

    void saveMatches(List<Match> matches);

    List<String> getMatchUniqueIds();

    int countDuplicateMatchIds(List<String> matchIds);

    void calcGamesCounts();

    Map<String, Map<LocalDateTime, Integer>> getGamesCounts();

    Set<String> getSavedMatchesUrls(List<String> urls);
}
