package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class Location {
    private final String country;
    private final Region region;

    public enum Region {
        AFRICA, ASIA, CHINA, CIS, EUROPE,
        NORTH_AMERICA, OCEANIA, SOUTH_AMERICA
    }
}
