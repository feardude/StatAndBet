package ru.mskgroup.statandbet.hltv.loader.parsing;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jsoup.nodes.Document;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ParserUtil {
    static boolean pageLoadedWithError(Document html) {
        return html.body().className().equalsIgnoreCase("error-body");
    }
}
