package ru.mskgroup.statandbet.hltv.calculator;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto.TeamOutcome;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
@NoArgsConstructor
@Setter
public class RatingCalculator {
    private static final double DEFAULT_RATING = 1000d;

    private Map<String, Map<LocalDateTime, Integer>> gamesCountByDatetimeAndTeam;

    public List<Rating> calculateRatings(List<Rating> oldRatings, List<MatchOutcomeDto> matchOutcomeDtos) {
        Map<String, Double> ratingByTeam = oldRatings.stream()
                .collect(toMap(
                        Rating::getTeamName,
                        Rating::getValue)
                );

        matchOutcomeDtos.forEach(
                match -> updateRatings(ratingByTeam, match)
        );

        return ratingByTeam.entrySet().stream()
                .map(entry -> Rating.builder()
                        .teamName(entry.getKey())
                        .value(entry.getValue())
                        .build()
                )
                .collect(toList());
    }

    private void updateRatings(Map<String, Double> ratingByTeam, MatchOutcomeDto match) {
        TeamOutcome teamOutcome1 = match.getTeamOutcome1();
        TeamOutcome teamOutcome2 = match.getTeamOutcome2();
        double oldRating1 = ratingByTeam.getOrDefault(teamOutcome1.getName(), DEFAULT_RATING);
        double oldRating2 = ratingByTeam.getOrDefault(teamOutcome2.getName(), DEFAULT_RATING);

        double newRating1 = evalNewRating(match.getMatchDateTime(), oldRating1, oldRating2, teamOutcome1);
        double newRating2 = evalNewRating(match.getMatchDateTime(), oldRating2, oldRating1, teamOutcome2);
        ratingByTeam.put(teamOutcome1.getName(), newRating1);
        ratingByTeam.put(teamOutcome2.getName(), newRating2);
    }

    private double evalNewRating(LocalDateTime matchDateTime, double rating1, double rating2, TeamOutcome team) {
        int gamesCount = gamesCountByDatetimeAndTeam.get(team.getName())
                    .getOrDefault(matchDateTime, 0);
        double emoFactor = calcEmoFactor(gamesCount);
        return rating1 + emoFactor * (team.getOutcome().getFactor() - calcExpectation(rating1, rating2));
    }

    // Mathematical expectation
    private double calcExpectation(double rating1, double rating2) {
        return rating1 / (rating1 + rating2);
    }

    private int calcEmoFactor(int gamesCount) {
        // TODO ступени steps
        if (gamesCount < 10) {
            return 100;
        } else if (gamesCount < 100) {
            return 10;
        } else if (gamesCount < 1000) {
            return 5;
        } else {
            return 1;
        }
    }
}
