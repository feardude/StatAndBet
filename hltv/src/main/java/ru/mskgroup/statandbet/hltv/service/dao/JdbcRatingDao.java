package ru.mskgroup.statandbet.hltv.service.dao;

import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;

import java.util.List;

@Repository
@AllArgsConstructor
public class JdbcRatingDao implements RatingDao {
    private static final String SAVE =
            "INSERT INTO rating (team_id, value)" +
            "VALUES (" +
            "  (SELECT id " +
            "   FROM team " +
            "   WHERE name = :teamName)," +
            "  :value" +
            ")" +
            "ON CONFLICT (team_id)" +
            "  DO UPDATE SET value = :value";

    private static final String GET_ALL =
            "SELECT t.name as team_name, value " +
            "FROM rating " +
            "JOIN team t ON rating.team_id = t.id";

    private final NamedParameterJdbcOperations jdbcTemplate;

    @Override
    public List<Rating> getRatings() {
        return jdbcTemplate.query(GET_ALL, toTeamRating());
    }

    @Override
    public void saveRatings(List<Rating> ratings) {
        val batchArgs = ratings.stream()
                .map(this::ratingParams)
                .toArray(SqlParameterSource[]::new);
        jdbcTemplate.batchUpdate(SAVE, batchArgs);
    }


    private SqlParameterSource ratingParams(Rating rating) {
        return new MapSqlParameterSource()
                .addValue("teamName", rating.getTeamName())
                .addValue("value", rating.getValue());
    }

    private RowMapper<Rating> toTeamRating() {
        return (rs, rowNum) -> Rating.builder()
                .teamName(rs.getString("team_name"))
                .value(rs.getDouble("value"))
                .build();
    }
}
