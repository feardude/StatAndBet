package ru.mskgroup.statandbet.hltv.model.calculator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import ru.mskgroup.statandbet.hltv.model.MatchOutcome;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class MatchOutcomeDto {
    private final TeamOutcome teamOutcome1;
    private final TeamOutcome teamOutcome2;
    private final LocalDateTime matchDateTime;

    @AllArgsConstructor
    @Builder
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class TeamOutcome {
        private final String name;
        private final MatchOutcome outcome;
    }
}
