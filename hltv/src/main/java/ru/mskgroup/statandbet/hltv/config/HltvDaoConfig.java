package ru.mskgroup.statandbet.hltv.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.mskgroup.statandbet.hltv.service.dao.JdbcMatchDao;
import ru.mskgroup.statandbet.hltv.service.dao.JdbcRatingDao;
import ru.mskgroup.statandbet.hltv.service.dao.MatchDao;
import ru.mskgroup.statandbet.hltv.service.dao.RatingDao;

import javax.sql.DataSource;

@PropertySource("classpath:datasource.properties")
@Configuration
@EnableTransactionManagement
class HltvDaoConfig {
    @Value("${hltv.jdbc.driver}")
    private String driver;
    @Value("${hltv.jdbc.url}")
    private String url;
    @Value("${hltv.jdbc.username}")
    private String username;
    @Value("${hltv.jdbc.password}")
    private String password;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public BasicDataSource dataSource() {
        return configureDataSource(url, username, password);
    }

    private BasicDataSource configureDataSource(String url, String username, String password) {
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setInitialSize(20);
        ds.setMaxActive(10);
        return ds;
    }

    @Bean
    public NamedParameterJdbcOperations namedJdbcTemplate(@Qualifier("dataSource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public MatchDao matchDao(NamedParameterJdbcOperations namedJdbcTemplate) {
        return new JdbcMatchDao(namedJdbcTemplate);
    }

    @Bean
    public RatingDao ratingDao(NamedParameterJdbcOperations namedJdbcTemplate) {
        return new JdbcRatingDao(namedJdbcTemplate);
    }

    @Bean
    public PlatformTransactionManager platformTransactionManager(BasicDataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
