package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class Tournament {
    private final String name;
    private final Location location;
    private final Status status;
    private final PrizePool prizePool;

    public enum Status {
        SHOWMATCH,
        WEEKLY,
        QUALIFIER,
        MONTHLY,
        MINOR,
        MAJOR,
        PREMIER
    }
}
