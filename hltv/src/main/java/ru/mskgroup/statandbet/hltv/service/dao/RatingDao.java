package ru.mskgroup.statandbet.hltv.service.dao;

import ru.mskgroup.statandbet.hltv.model.calculator.Rating;

import java.util.List;

public interface RatingDao {
    List<Rating> getRatings();

    void saveRatings(List<Rating> ratings);
}
