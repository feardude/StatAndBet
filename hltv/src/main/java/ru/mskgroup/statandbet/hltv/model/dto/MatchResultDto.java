package ru.mskgroup.statandbet.hltv.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import ru.mskgroup.statandbet.hltv.model.MatchOutcome;

@AllArgsConstructor
@Builder
@Getter
public class MatchResultDto {
    private final int teamId;
    private final int score;
    private final MatchOutcome outcome;
}
