package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Builder
@Getter
public class Team {
    private final String country;
    private final LocalDate createdOn;
    private final String name;
    private final String url;
}
