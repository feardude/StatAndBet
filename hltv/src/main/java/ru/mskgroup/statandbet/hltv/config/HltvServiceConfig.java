package ru.mskgroup.statandbet.hltv.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.mskgroup.statandbet.hltv.service.dao.MatchDao;
import ru.mskgroup.statandbet.hltv.service.dao.RatingDao;
import ru.mskgroup.statandbet.hltv.service.HltvService;
import ru.mskgroup.statandbet.hltv.service.HltvServiceImpl;

@Configuration
@Import(HltvDaoConfig.class)
public class HltvServiceConfig {
    @Bean
    public HltvService hltvService(MatchDao matchDao, RatingDao ratingDao) {
        return new HltvServiceImpl(matchDao, ratingDao);
    }
}
