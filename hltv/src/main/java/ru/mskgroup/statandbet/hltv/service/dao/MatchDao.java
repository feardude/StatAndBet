package ru.mskgroup.statandbet.hltv.service.dao;

import ru.mskgroup.statandbet.hltv.model.GamesCount;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.dto.TeamMatchDates;
import ru.mskgroup.statandbet.hltv.model.parser.Match;

import java.util.List;
import java.util.Set;

public interface MatchDao {
    List<Match> getAll();

    void save(List<Match> matches);

    List<String> getMatchUniqueIds();

    int countDuplicateMatchIds(List<String> matchIds);

    List<MatchOutcomeDto> getMatchOutcomeDtos();

    void saveGamesCount(List<GamesCount> gamesCounts);

    List<GamesCount> getGamesCount();

    List<TeamMatchDates> getTeamGames();

    void deleteMatches(List<Integer> hltvMatchIds);

    void truncateGamesCount();

    Set<String> getSavedMatchesUrls(List<String> urls);
}
