package ru.mskgroup.statandbet.hltv.loader.parsing;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;
import ru.mskgroup.commons.JsoupUtil;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.currentTimeMillis;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static ru.mskgroup.statandbet.hltv.loader.parsing.ParserUtil.pageLoadedWithError;

@Component
@AllArgsConstructor
@Slf4j
public class ResultsParser {
    private final String resultsUrlPattern;
    private final List<Integer> years;

    public List<MatchUrl> parse() {
        return years.parallelStream()
                .map(this::doParse)
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private List<MatchUrl> doParse(int year) {
        final long startTime = currentTimeMillis();
        log.info("{} year: started collecting match URLs", year);

        final List<MatchUrl> matchUrls = new ArrayList<>(1000);
        String resultsPageUrl = String.format(resultsUrlPattern, year, year);
        do {
            final Document resultsHtml = JsoupUtil.getHtmlFromURL("https://www.hltv.org" + resultsPageUrl);
            if (isNull(resultsHtml)) {
                return null;
            }

            matchUrls.addAll(getMatchUrls(resultsHtml));
            resultsPageUrl = getNextResultsPageUrl(resultsHtml, resultsPageUrl);
        } while (!resultsPageUrl.isEmpty());

        final long workTime = calcWorkTime(startTime, currentTimeMillis());
        log.info("{} year: collected {} URLs in {} seconds", year, matchUrls.size(), workTime);
        return matchUrls;
    }

    public List<MatchUrl> getMatchUrls(Document html) {
        if (!pageLoadedWithError(html)) {
            return parseCurrentPageMatchUrls(html);
        }
        log.warn("Page was loaded with error. Parsing can't be executed, skipping these matches.");
        return emptyList();
    }

    public String getNextResultsPageUrl(Document html, String lastNextPageUrl) {
        if (pageLoadedWithError(html)) {
            log.warn("Calculating next page getManualNextPageUrl manually.");
            return getManualNextPageUrl(lastNextPageUrl);
        }
        return html.getElementsByClass("pagination-next").get(0).attr("href");
    }

    private List<MatchUrl> parseCurrentPageMatchUrls(Document html) {
        return html.getElementsByClass("result-con")
                .stream()
                .map(this::toMatchUrl)
                .distinct()
                .filter(matchUrl -> !matchUrl.getUrl().isEmpty())
                .filter(differentTeamNames())
                .collect(toList());
    }

    private Predicate<MatchUrl> differentTeamNames() {
        return matchUrl -> !matchUrl.getTeam1()
                .equalsIgnoreCase(matchUrl.getTeam2());
    }

    private MatchUrl toMatchUrl(Element row) {
        final List<String> teams = row.getElementsByClass("team").eachText();
        return MatchUrl.builder()
                .url(row.getElementsByTag("a").attr("href"))
                .team1(teams.get(0))
                .team2(teams.get(1))
                .build();
    }

    private String getManualNextPageUrl(String url) {
        final String urlPatternRegex = "(.*offset=)(\\d+)(.*)";
        final Matcher matcher = Pattern.compile(urlPatternRegex).matcher(url);
        if (matcher.find()) {
            final String offset = matcher.group(2);
            final Integer newOffset = Integer.parseInt(offset) + 100;
            return url.replaceFirst(urlPatternRegex, "$1" + newOffset.toString() + "$3");
        }
        return null;
    }

    private long calcWorkTime(long startTime, long endTime) {
        return (endTime - startTime) / 1000;
    }
}
