package ru.mskgroup.statandbet.hltv.model.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
public class MatchUrl {
    private final String url;
    private final String team1;
    private final String team2;
}
