package ru.mskgroup.statandbet.hltv.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.mskgroup.statandbet.hltv.model.GamesCount;
import ru.mskgroup.statandbet.hltv.model.calculator.MatchOutcomeDto;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;
import ru.mskgroup.statandbet.hltv.model.dto.TeamMatchDates;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.service.dao.MatchDao;
import ru.mskgroup.statandbet.hltv.service.dao.RatingDao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Component
@AllArgsConstructor
@Slf4j
public class HltvServiceImpl implements HltvService {
    private final MatchDao matchDao;
    private final RatingDao ratingDao;

    @Override
    public List<Rating> getRatings() {
        return ratingDao.getRatings();
    }

    @Override
    @Transactional
    public void saveRatings(List<Rating> ratings) {
        ratingDao.saveRatings(ratings);
    }

    @Override
    public List<MatchOutcomeDto> getMatchOutcomeDtos() {
        return matchDao.getMatchOutcomeDtos();
    }

    @Override
    @Transactional
    public void saveMatches(List<Match> matches) {
        matchDao.save(matches);
    }

    @Override
    public List<String> getMatchUniqueIds() {
        return matchDao.getMatchUniqueIds();
    }

    @Override
    public int countDuplicateMatchIds(List<String> matchIds) {
        return matchDao.countDuplicateMatchIds(matchIds);
    }

    @Override
    @Transactional
    public void calcGamesCounts() {
        log.info("Truncating old games counts");
        matchDao.truncateGamesCount();

        log.info("Calculating games counts");
        final List<GamesCount> gamesCounts = matchDao.getTeamGames().stream()
                .map(this::countGamesForTeam)
                .flatMap(Collection::stream)
                .collect(toList());
        matchDao.saveGamesCount(gamesCounts);
    }

    @Override
    public Map<String, Map<LocalDateTime, Integer>> getGamesCounts() {
        return matchDao.getGamesCount().stream()
                .collect(groupingBy(
                        GamesCount::getTeamName
                )).entrySet().stream()
                .map(entry -> singletonMap(
                        entry.getKey(),
                        toGamesCountByDatetimeMap(entry.getValue())
                ))
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }

    @Override
    public Set<String> getSavedMatchesUrls(List<String> urls) {
        return matchDao.getSavedMatchesUrls(urls);
    }

    private List<GamesCount> countGamesForTeam(TeamMatchDates dto) {
        List<LocalDateTime> matchDates = dto.getMatchDates();
        int totalMatches = matchDates.size();
        List<GamesCount> gamesCount = new ArrayList<>(totalMatches);

        int count = 1;
        for (LocalDateTime matchDate : matchDates) {
            gamesCount.add(
                    GamesCount.builder()
                            .teamName(dto.getTeamName())
                            .validOn(matchDate.plusMinutes(1))  //TODO remove plusMinutes(1)
                            .count(count++)
                            .build()
            );
        }
        return gamesCount;
    }

    private Map<LocalDateTime, Integer> toGamesCountByDatetimeMap(List<GamesCount> gamesCounts) {
        return gamesCounts.stream()
                .collect(toMap(
                        GamesCount::getValidOn,
                        GamesCount::getCount
                ));
    }
}
