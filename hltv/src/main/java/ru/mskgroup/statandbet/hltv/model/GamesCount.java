package ru.mskgroup.statandbet.hltv.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class GamesCount {
    private final String teamName;
    private final LocalDateTime validOn;
    private final int count;
}
