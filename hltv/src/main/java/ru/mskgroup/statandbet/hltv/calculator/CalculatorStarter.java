package ru.mskgroup.statandbet.hltv.calculator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import ru.mskgroup.statandbet.hltv.config.HltvServiceConfig;
import ru.mskgroup.statandbet.hltv.model.calculator.Rating;
import ru.mskgroup.statandbet.hltv.service.HltvService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@Import(HltvServiceConfig.class)
@Slf4j
public class CalculatorStarter {
    private final RatingCalculator ratingCalculator;
    private final HltvService hltvService;

    public CalculatorStarter(RatingCalculator ratingCalculator, HltvService hltvService) {
        this.hltvService = hltvService;
        this.ratingCalculator = ratingCalculator;
    }

    public static void main(String[] args) {
        try {
            log.info("Starting MMR calculator...");
            SpringApplication.run(CalculatorStarter.class);
            log.info("MMR calculator started");
        } catch (Exception e) {
            log.error("MMR calculator failed.", e);
        }
        log.info("MMRs were calculated, closing MMR calculator.");
    }

    @Bean
    public CommandLineRunner run() {
        return args -> {
            Map<String, Map<LocalDateTime, Integer>> gamesCountByDatetimeAndTeam = hltvService.getGamesCounts();
            ratingCalculator.setGamesCountByDatetimeAndTeam(gamesCountByDatetimeAndTeam);
            List<Rating> updatedRatings = ratingCalculator.calculateRatings(
                    hltvService.getRatings(),
                    hltvService.getMatchOutcomeDtos()
            );
            hltvService.saveRatings(updatedRatings);
        };
    }
}
