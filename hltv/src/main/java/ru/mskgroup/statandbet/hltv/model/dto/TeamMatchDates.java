package ru.mskgroup.statandbet.hltv.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class TeamMatchDates {
    private final String teamName;
    private final List<LocalDateTime> matchDates;
}
