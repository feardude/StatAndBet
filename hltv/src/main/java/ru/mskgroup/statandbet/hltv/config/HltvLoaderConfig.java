package ru.mskgroup.statandbet.hltv.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Configuration
@PropertySource("classpath:hltv.properties")
@Import(HltvServiceConfig.class)
public class HltvLoaderConfig {
    @Value("${hltv.loader.loadFullHistory}")
    private boolean loadFullHistory;

    @Value("${hltv.loader.minYear}")
    private int minYear;

    @Value("${hltv.loader.results.urlPattern}")
    private String resultsUrlPattern;

    @Bean
    public String resultsUrlPattern() {
        return resultsUrlPattern;
    }

    @Bean
    public List<Integer> years() {
        final int currentYear = LocalDate.now().getYear();
        return !loadFullHistory ? singletonList(currentYear) :
                IntStream.rangeClosed(minYear, currentYear)
                        .boxed()
                        .collect(toList());
    }
}
