package ru.mskgroup.statandbet.hltv.model.parser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import ru.mskgroup.statandbet.hltv.model.MatchOutcome;

@AllArgsConstructor
@Builder
@Getter
@EqualsAndHashCode
@ToString
public class MatchResult {
    private final String teamName;
    private final int score;
    private final MatchOutcome outcome;
}
