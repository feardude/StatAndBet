package ru.mskgroup.statandbet.hltv.loader;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchParser;
import ru.mskgroup.statandbet.hltv.loader.parsing.MatchPersistence;
import ru.mskgroup.statandbet.hltv.loader.parsing.ResultsParser;
import ru.mskgroup.statandbet.hltv.model.parser.Match;
import ru.mskgroup.statandbet.hltv.model.parser.MatchUrl;
import ru.mskgroup.statandbet.hltv.service.HltvService;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

@Component
@AllArgsConstructor
@Slf4j
public class HltvLoader {
    private static final String BASE_URL = "https://www.hltv.org";

    private final HltvService hltvService;
    private final MatchParser matchParser;
    private final MatchPersistence matchPersistence;
    private final ResultsParser resultsParser;

    public void load() {
        final List<MatchUrl> matchUrls = resultsParser.parse();
        log.info("Total match URLs to be parsed: {}", matchUrls.size());

        final List<MatchUrl> filteredMatchUrls = filterSavedMatches(matchUrls);
        log.info("Filtered match URLs to be parsed: {}", filteredMatchUrls.size());

        if (filteredMatchUrls.isEmpty()) {
            log.info("No new matches, exiting now");
            return;
        }

        final List<Match> matches = matchParser.parse(filteredMatchUrls);
        log.info("Total matches parsed: {}", matches.size());

        matchPersistence.save(matches);
        log.info("Matches were saved successfully");

        hltvService.calcGamesCounts();
        log.info("Games counts were calculated successfully");
    }

    private List<MatchUrl> filterSavedMatches(List<MatchUrl> matchUrls) {
        final List<String> urls = matchUrls.stream()
                .map(MatchUrl::getUrl)
                .map(BASE_URL::concat)
                .collect(toList());
        log.info("Collected match urls: {}", urls.size());

        final Set<String> savedUrls = hltvService.getSavedMatchesUrls(urls);
        log.info("Saved match urls: {}", savedUrls.size());

        return matchUrls.stream()
                .filter(retainUnsavedMatchUrls(savedUrls))
                .collect(toList());
    }

    private Predicate<MatchUrl> retainUnsavedMatchUrls(Set<String> savedUrls) {
        return m -> !savedUrls.contains(BASE_URL.concat(m.getUrl()));
    }
}
